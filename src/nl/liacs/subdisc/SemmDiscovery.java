package nl.liacs.subdisc;

import java.util.*;
import java.util.concurrent.atomic.*;

import matlabcontrol.*;
import siemm.*;

public class SemmDiscovery {
	private SearchParameters searchParameters;
	private Table table;
	private SubgroupSet results;
	private CandidateQueue candidateQueue;
	private AtomicInteger candidateCount = new AtomicInteger(0);
	private MatlabProxy proxy;
	private double[][] target;
	private Object[] equiClasses;
	private int d;
	private int n;
	private int id;
	private int[] targetIdxs;
	private Map<Integer, double[]> ws;
	private Map<Integer, double[]> attributeSIs;

	private String siScriptName;
	private String updateScriptName;
//	private float p;
//	private int inputDim;
	private int iteration;
	
	public SemmDiscovery(SearchParameters searchParameters, Table table, int[] targetIdxs) {
		
		this.searchParameters = searchParameters;
		this.table = table;
//		this.results = new SubgroupSet(this.searchParameters.getMaximumSubgroups(), this.table.getNrRows());
		this.results = new SubgroupSet(this.table.getNrRows());
		
		this.id = 0;
		this.ws = new HashMap<>();		
		this.attributeSIs = new HashMap<>();
		
		this.n = this.table.getNrRows();
		this.d = targetIdxs.length;
		this.targetIdxs = targetIdxs;
		this.iteration = 0;
		
		target = new double[this.n][this.d];
		for (int j = 0; j < this.d; j++) {		
			int targetIdx = targetIdxs[j];
			boolean isBinaryCol = (table.getColumn(targetIdx).getType() == AttributeType.BINARY);
			for (int i = 0; i < this.n; i++) {
				if (isBinaryCol) {
					target[i][j] = table.getColumn(targetIdx).getBinary(i)?1.0:0.0;
				} else {
					target[i][j] = table.getColumn(targetIdx).getFloat(i);
				}
			}
		}
//		inputDim = this.table.getNrColumns() - this.d;
//		p = (float) (1./inputDim);
		
		MatlabProxyFactory factory = new MatlabProxyFactory();			
		
		try {
			this.proxy = factory.getProxy();			
			proxy.eval("addpath src/siemm/");
			proxy.eval("addpath ./manopt; importmanopt;");
			Object[] para = proxy.returningFeval("init", 1, target);
			equiClasses = (Object[]) para[0];	
		} catch (MatlabInvocationException | MatlabConnectionException e) {
			e.printStackTrace();
		}		
			
	}
	
	public void mineLocationPatterns() {
		this.iteration += 1;		
		this.siScriptName = "siDisplacement";
		this.updateScriptName = "updateDisplacement";
		resetBuffersAtBeginingOfEachIteration();
		mine(System.currentTimeMillis());
	}
		
	public void findWeightVectorForLocationPatternSparse(Subgroup selectedPattern) {
		this.siScriptName = "siSpread_sparse";
		this.updateScriptName = "updateSpread";

		Triplet result = evaluateCandidate(selectedPattern);
		this.ws.put(selectedPattern.getID(), result.w);
		this.attributeSIs.put(selectedPattern.getID(), result.attrVs);
		selectedPattern.setMeasureValue(result.v);
	}
	
	public void findWeightVectorForLocationPatternNonSparse(Subgroup selectedPattern) {
		this.siScriptName = "siSpread_nonsparse";		
		this.updateScriptName = "updateSpread";

		Triplet result = evaluateCandidate(selectedPattern);
		this.ws.put(selectedPattern.getID(), result.w);
		this.attributeSIs.put(selectedPattern.getID(), result.attrVs);
		selectedPattern.setMeasureValue(result.v);
	}
	
	public void finish() {
		try {
			proxy.exit();
		} catch (MatlabInvocationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void updateBackgroundDistribution(Subgroup selectedSG) {		
		Log.logCommandLine("You selected pattern id." + selectedSG.getID() + " desc: " + selectedSG.toString());
		BitSet memebers = selectedSG.getMembers();
		int[] idxs = new int[memebers.cardinality()];
		int idx = 0;
		for (int i = memebers.nextSetBit(0); i != -1; i = memebers.nextSetBit(i + 1)) {
		    idxs[idx] = i;
		    idx++;
		}
		if (selectedSG != null) {			
			try {
				Object[] para = proxy.returningFeval(updateScriptName, 1, equiClasses, target, idxs, this.ws.get(selectedSG.getID()));
				this.equiClasses = (Object[]) para[0];
			} catch (MatlabInvocationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} 
	}
	
	public void updateBackgroundDistribution(List<Subgroup> subgroups) {	
		Subgroup selectedSG = subgroups.get(subgroups.size() - 1); 
		Log.logCommandLine("You selected pattern id." + selectedSG.getID() + " desc: " + selectedSG.toString());
		for (int k = 0; k < 2; k++) {
			for (int i = 0; i < subgroups.size(); i++) {
				Subgroup subgroup = subgroups.get(i);
				BitSet memebers = subgroup.getMembers();
				int[] idxs = new int[memebers.cardinality()];
				int idx = 0;
				for (int j = memebers.nextSetBit(0); j != -1; j = memebers.nextSetBit(j + 1)) {
				    idxs[idx] = j;
				    idx++;
				}
				if (subgroup != null) {			
					try {
						Object[] para = proxy.returningFeval(updateScriptName, 1, equiClasses, target, idxs, null);
						this.equiClasses = (Object[]) para[0];
					} catch (MatlabInvocationException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				} 
			}
		}
	}
	
	public void updateBackgroundDistribution(List<Subgroup> subgroups, List<double[]> selectedWs) {		
		Subgroup selectedSG = subgroups.get(subgroups.size() - 1); 
		Log.logCommandLine("You selected pattern id." + selectedSG.getID() + " desc: " + selectedSG.toString());
		for (int k = 0; k < 2; k++) {
			for (int i = 0; i < subgroups.size(); i++) {
				Subgroup subgroup = subgroups.get(i);
				double[] w = selectedWs.get(i);
				BitSet memebers = subgroup.getMembers();
				int[] idxs = new int[memebers.cardinality()];
				int idx = 0;
				for (int j = memebers.nextSetBit(0); j != -1; j = memebers.nextSetBit(j + 1)) {
				    idxs[idx] = j;
				    idx++;
				}
				if (subgroup != null) {			
					try {
						Object[] para = proxy.returningFeval(updateScriptName, 1, equiClasses, target, idxs, w);
						this.equiClasses = (Object[]) para[0];
					} catch (MatlabInvocationException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				} 
			}
		}
	}
	
	private void resetBuffersAtBeginingOfEachIteration() {
//		this.results = new SubgroupSet(this.searchParameters.getMaximumSubgroups(), this.table.getNrRows());
		this.results = new SubgroupSet(this.table.getNrRows());
		this.ws = new HashMap<>();
		this.attributeSIs = new HashMap<>();
	}
	
	private void mine(long beginTime) {
		// make subgroup to start with, containing all elements
		BitSet bitSet = new BitSet(this.table.getNrRows());
		bitSet.set(0, this.table.getNrRows());
//		Subgroup aStart = new Subgroup(ConditionListBuilder.emptyList(), bitSet, results);
		Subgroup aStart = new Subgroup(null, bitSet, results);
		candidateQueue = new CandidateQueue(searchParameters, new Candidate(aStart));

		long endTime = beginTime + (((long) searchParameters.getMaximumTime()) * 60 * 1000);
		if (endTime <= beginTime)
			endTime = Long.MAX_VALUE;

		int searchDepth = searchParameters.getSearchDepth();
//		final ConditionBaseSet aConditions = new ConditionBaseSet(table, searchParameters);
		while ((candidateQueue.size() > 0) && (System.currentTimeMillis() <= endTime)) {
			Candidate aCandidate = candidateQueue.removeFirst();
			Subgroup aSubgroup = aCandidate.getSubgroup();

			if (aSubgroup.getDepth() < searchDepth) {
//				RefinementList aRefinementList = new RefinementList(aSubgroup, aConditions);
				RefinementList aRefinementList = new RefinementList(aSubgroup, table, searchParameters);

				final BitSet members = aSubgroup.getMembers();

				for (int i = 0, j = aRefinementList.size(); i < j; i++) {
					if (System.currentTimeMillis() > endTime)
						break;

					Refinement aRefinement = aRefinementList.get(i);
//					ConditionBase aConditionBase = aRefinement.getConditionBase();
					Condition aCondition = aRefinement.getCondition();
//					if (aConditionBase.getColumn().getType() == AttributeType.NUMERIC
//							&& aConditionBase.getOperator() != Operator.EQUALS)
					if (aCondition.getColumn().getType() == AttributeType.NUMERIC && aCondition.getOperator() != Operator.EQUALS)						
						evaluateNumericRefinements(members, aRefinement);
					else
						evaluateNominalBinaryRefinements(members, aRefinement);
				}
			}
		}
		Log.logCommandLine("number of candidates: " + candidateCount.get());
		Log.logCommandLine("number of subgroups: " + results.size());

		// log
		long anEnd = System.currentTimeMillis();
		float aMaxTime = searchParameters.getMaximumTime();

		if (aMaxTime > 0.0f && (anEnd > (beginTime + aMaxTime * 60 * 1000))) {
			String aMessage = "Mining process ended prematurely due to time limit.";
			Log.logCommandLine(aMessage);
		}

		logResult();

		echoMiningEnd(anEnd - beginTime, results.size());
	}

	private void echoMiningEnd(long theMilliSeconds, int theNumberOfSubgroups) {
		int seconds = Math.round(theMilliSeconds / 1000);
		int minutes = Math.round(theMilliSeconds / 60000);
		int secondsRemainder = seconds - (minutes * 60);
		String aString = new String(
				"Mining process finished in " + minutes + " minutes and " + secondsRemainder + " seconds.\n");

		if (theNumberOfSubgroups == 0)
			aString += "   No subgroups found that match the search criterion.\n";
		else if (theNumberOfSubgroups == 1)
			aString += "   1 subgroup found.\n";
		else
			aString += "   " + theNumberOfSubgroups + " subgroups found.\n";
		Log.logCommandLine(aString);
	}

	private void evaluateNumericRefinements(BitSet members, Refinement refinement) {
		
		
		switch (searchParameters.getNumericStrategy())
		{			
//			case NUMERIC_VIKAMINE_CONSECUTIVE_ALL :
//			{							
//				Subgroup anOldSubgroup = refinement.getSubgroup();
//				// faster than theMembers.cardinality()
//				// useless call, coverage never changes, could be parameter
//				final int anOldCoverage = anOldSubgroup.getCoverage();
//				assert (members.cardinality() == anOldCoverage);
//
//				// see comment at evaluateNominalBinaryRefinement()
//				ConditionBase aConditionBase = refinement.getConditionBase();
//
//				// this is the crucial translation from nr bins to nr splitpoints
//				int aNrSplitPoints = searchParameters.getNrBins() - 1;
//				// useless, Column.getSplitPointsBounded() would return an empty array
//				if (aNrSplitPoints <= 0)
//					return;
//				Interval[] anIntervals = aConditionBase.getColumn().getSplitPointsBounded(members, aNrSplitPoints);
//
//				for (int i = 0; i < anIntervals.length; ++i)
//				{
//					Interval anInterval = anIntervals[i];
//
//					// catch invalid input
//					if (anInterval == null)
//						continue;
//					// catch duplicates
//					if ((i > 0) && (anInterval.compareTo(anIntervals[i-1]) == 0))
//						continue;
//
//					Subgroup aNewSubgroup = refinement.getRefinedSubgroup(anInterval);
//
//					checkAndLog(aNewSubgroup, anOldCoverage);
//				}				
//
//				float[] splitPoints = getUniqueSplitPoints(members, aConditionBase.getColumn(),
//						searchParameters.getNrBins());
//
//				for (float splitPoint : splitPoints) {
//					try{
//						Subgroup newSubgroup = refinement.getRefinedSubgroup(splitPoint);
//						checkAndLog(newSubgroup, anOldCoverage);
//					} catch	(IllegalArgumentException e) {
//						continue;
//					}					
//				}
//				
//				break;
//			}
			
			default :
			{
				final int oldCoverage = refinement.getSubgroup().getCoverage();

//				ConditionBase aConditionBase = refinement.getConditionBase();

				float[] splitPoints = getUniqueSplitPoints(members, refinement.getCondition().getColumn(),
						searchParameters.getNrBins());

				for (float splitPoint : splitPoints) {
					Subgroup newSubgroup = refinement.getRefinedSubgroup(Float.toString(splitPoint));
					checkAndLog(newSubgroup, oldCoverage);
				}

				break;
			}
		}
	}

	private void evaluateNominalBinaryRefinements(BitSet members, Refinement refinement) {
		final int oldCoverage = refinement.getSubgroup().getCoverage();
//		final ConditionBase conditionBase = refinement.getConditionBase();
		final Condition condition = refinement.getCondition();
		if (condition.getOperator() == Operator.ELEMENT_OF) {

		} else { //regular single-value conditions
			// members-based domain, no empty Subgroups will occur
			Column c = condition.getColumn();
			Subgroup aNewSubgroup = null;

			// switch for now, will separate code paths when numeric EQUALS is fixed
			switch (c.getType())
			{
				case NOMINAL :
				{
					for (String aValue : c.getUniqueNominalBinaryDomain(members))
					{
						aNewSubgroup = refinement.getRefinedSubgroup(aValue);
						checkAndLog(aNewSubgroup, oldCoverage);
					}
					break;
				}
				case NUMERIC :
				{
					for (float aValue : c.getUniqueNumericDomain(members))
					{
						aNewSubgroup = refinement.getRefinedSubgroup(Float.toString(aValue));
						checkAndLog(aNewSubgroup, oldCoverage);
					}
					break;
				}
				case ORDINAL :
					throw new AssertionError(AttributeType.ORDINAL);
				case BINARY :
				{
					for (String aValue : condition.getColumn().getDomain())
					{
						aNewSubgroup = refinement.getRefinedSubgroup(aValue);
						checkAndLog(aNewSubgroup, oldCoverage);
					}
				}
				default :
					for (String aValue : condition.getColumn().getDomain())
					{
						aNewSubgroup = refinement.getRefinedSubgroup(aValue);
						checkAndLog(aNewSubgroup, oldCoverage);
					}
			}
		}
	}

	private void checkAndLog(Subgroup subgroup, int theOldCoverage) {
		final int aNewCoverage = subgroup.getCoverage();

		if (aNewCoverage < theOldCoverage && aNewCoverage >= searchParameters.getMinimumCoverage()) {
			subgroup.setID(id++);
			Triplet p = evaluateCandidate(subgroup);
			float aQuality = p.v;
			subgroup.setMeasureValue(aQuality);

			// if the quality is enough, or should be ignored, ...
			if (aQuality > searchParameters.getQualityMeasureMinimum()) {
				// ...and, the coverage is not too high
				if (aNewCoverage <= (int) (table.getNrRows() * searchParameters.getMaximumCoverageFraction())) {					
					if (results.add(subgroup)) {
						ws.put(subgroup.getID(), p.w);
						attributeSIs.put(subgroup.getID(), p.attrVs);
					};
				}
			}

			candidateQueue.add(new Candidate(subgroup));

			// logCandidateAddition(subgroup);
		}
		candidateCount.getAndIncrement();
	}

	public Triplet evaluateCandidate(Subgroup newSubgroup) {
		BitSet memebers = newSubgroup.getMembers();
		double[] idxs = new double[memebers.cardinality()];
		int idx = 0;
		for (int i = memebers.nextSetBit(0); i != -1; i = memebers.nextSetBit(i + 1)) {
		    idxs[idx] = i;
		    idx++;
		}
		
		float v = (float) 0.0;
		double [] w = null;
		double [] attrV = null;
		try {
			Object[] para = proxy.returningFeval(this.siScriptName, 3, idxs, target, equiClasses);			
			w = (double[]) para[0];
			v = (float) ((double[]) para[1])[0];
			attrV = (double[]) para[2];
			int nrConditions = newSubgroup.getConditions().size(); 
//			float desc = (float) -Math.log(Math.pow(p, nrConditions)*Math.pow(1-p, inputDim- nrConditions));
			float a = (float) 0.5;
			float b = (float) 1.0;
			float desc = (float) a*nrConditions + b; 
			v = v/desc;
		} catch (MatlabInvocationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 
		return new Triplet(w, v, attrV);
	}
	
	public Triplet evaluateCandidate(BitSet memebers, int nrConditions) {
		double[] idxs = new double[memebers.cardinality()];
		int idx = 0;
		for (int i = memebers.nextSetBit(0); i != -1; i = memebers.nextSetBit(i + 1)) {
		    idxs[idx] = i;
		    idx++;
		}
		
		float v = (float) 0.0;
		double [] w = null;
		double [] attrV = null;
		try {
			Object[] para = proxy.returningFeval(this.siScriptName, 3, idxs, target, equiClasses);			
			w = (double[]) para[0];
			v = (float) ((double[]) para[1])[0];
			attrV = (double[]) para[2];
//			float desc = (float) -Math.log(Math.pow(p, nrConditions)*Math.pow(1-p, inputDim- nrConditions));
			float a = (float) 0.5;
			float b = (float) 1.0;
			float desc = (float) a*nrConditions + b; 
			v = v/desc;
		} catch (MatlabInvocationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 
		return new Triplet(w, v, attrV);
	}
	
	private void logResult() {

		for (Subgroup theSubgroup : this.results) {
			StringBuffer sb = new StringBuffer(200);
			sb.append("s.id = ");
			sb.append(theSubgroup.getID());
			sb.append(" candidate ");
			sb.append(theSubgroup.getConditions());
			sb.append(" size: ");
			sb.append(theSubgroup.getCoverage());
			sb.append(" si: ");
			sb.append(theSubgroup.getMeasureValue());
			Log.logCommandLine(sb.toString());

		}
	}

	// TODO MM - hack: functionality should be in theColumn.getSplitPoints()
	private final float[] getUniqueSplitPoints(BitSet theMembers, Column theColumn, int theNrBins) {
		// this is the crucial translation from nrBins to nrSplitpoints
		int aNrSplitPoints = theNrBins - 1;

		float[] aSplitPoints = theColumn.getSplitPoints(theMembers, aNrSplitPoints);

		Set<Float> aUniqueValues = new TreeSet<Float>();
		for (float f : aSplitPoints)
			aUniqueValues.add(f);

		float[] aResult = new float[aUniqueValues.size()];
		int i = -1;
		for (Float f : aUniqueValues)
			aResult[++i] = f.floatValue();

		return aResult;
	}
	public SubgroupSet getResults() {
		return results;
	}

	public double[] getW(int patternId) {
		return this.ws.get(patternId);
	}
	
	public int[] getTargetIdxs() {
		return targetIdxs;
	}
	
	public Map<Integer, double[]> getAttribueSIs() {
		return attributeSIs;	
	}
	
	public int getIteration() {
		return iteration;
	}
	
	public Object[] getModelMeanAndVar() {
		Object[] para = null;
		try {
			para = proxy.returningFeval("getModelMeanAndVar", 1, this.equiClasses);
		} catch (MatlabInvocationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
		return para; 
	}	
	
	public Subgroup getPatternById(int id) {
		Subgroup selectedSG = null;
		for (Subgroup s : results) {
			if (s.getID() == id) {
				selectedSG = s;
				
				break;
			}
		}		
		return selectedSG;
	}
	
	public Subgroup getRankKPattern(int rank) {
		int count = 1;
		for (Subgroup subgroup : results) {			
			if (count == rank) {
				return subgroup;
			}
			count++;
		}
		return null;
	}
}
