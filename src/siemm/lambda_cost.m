function R = lambda_cost(alpha,supdate,w,s2,m)
    R = 0;
    for j=1:length(supdate)
        ni = length(supdate(j).inds);
        mui = supdate(j).mu;
        Si = supdate(j).S;
        val1 = w'*Si*w/(1+alpha*w'*Si*w);
        val2 = ((m-mui'*w)/(1+alpha*w'*Si*w))^2;
        R = R + ni*(val1 + val2 - s2);
    end
end
