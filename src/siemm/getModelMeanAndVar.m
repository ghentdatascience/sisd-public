function result = getModelMeanAndVar(keys, classes)    
    result = struct('mu', [], 'sigma', [], 'inds', []);
    for i = 1:length(classes)
        class = classes{i};
        for j = 1:length(class)
            sall(i).(keys{j}) = class{j};
        end        
        s.mu = sall(i).mu;    
        d = length(s.mu);
        s.sigma = reshape(sall(i).S, d, d)';
        s.inds = double(sall(i).inds);
        result(end+1, 1) = s;
    end
    result(1) = [];
end