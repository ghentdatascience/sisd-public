package siemm;

import java.io.*;
import java.util.*;

import nl.liacs.subdisc.SearchParameters;
import nl.liacs.subdisc.Table;
import nl.liacs.subdisc.Subgroup;
import nl.liacs.subdisc.SemmDiscovery;

public class Miner {
	private Table table;
	private int[] targetIdxs;
	private SearchParameters searchParameters;
	private int[] supportRowIdxs;
	private String pathToLogFile;
	private List<Subgroup> selectedPatterns;
	private List<double[]> selectedWs;

	public Miner(Table table, int[] targetIdxs, SearchParameters searchParameters, String pathToLogFile) {
		this.table = table;
		printTableInfo();
		this.targetIdxs = targetIdxs;
		this.searchParameters = searchParameters;
		this.pathToLogFile = pathToLogFile;
		this.selectedPatterns = new ArrayList<>();
		this.selectedWs = new ArrayList<>();

		supportRowIdxs = new int[table.getNrRows()];
		for (int i = 0; i < table.getNrRows(); i++) {
			supportRowIdxs[i] = i;
		}
	}

	public void mineTwoStepSpreadPatterns(int nrIter) throws FileNotFoundException {
		SemmDiscovery semmDiscovery = new SemmDiscovery(searchParameters, table, targetIdxs);
		Scanner reader = new Scanner(System.in); // Reading from System.in
		// start logger.
		PrintWriter writerAfterLoc = initLogger(pathToLogFile.replace(".m", "BeforeUpdate1.m"));
		PrintWriter writerSpread1 = initLogger(pathToLogFile.replace(".m", "AfterUpdate1.m"));
		PrintWriter writerSpread2 = initLogger(pathToLogFile.replace(".m", "AfterUpdate2.m"));
		
		for (int i = 0; i < nrIter; i++) {
			semmDiscovery.mineLocationPatterns();
			Subgroup selectedPattern = semmDiscovery.getRankKPattern(1);
			this.selectedPatterns.add(selectedPattern);
			logPattern(selectedPattern, writerAfterLoc, semmDiscovery);
			logModelMeanParamters(writerAfterLoc, semmDiscovery);
			semmDiscovery.updateBackgroundDistribution(selectedPatterns);
			semmDiscovery.findWeightVectorForLocationPatternNonSparse(selectedPattern);
			this.selectedWs.add(semmDiscovery.getW(selectedPattern.getID()));
			logPattern(selectedPattern, writerSpread1, semmDiscovery);
			logModelMeanParamters(writerSpread1, semmDiscovery);
			semmDiscovery.updateBackgroundDistribution(selectedPatterns, selectedWs);
			semmDiscovery.findWeightVectorForLocationPatternNonSparse(selectedPattern);

			logPattern(selectedPattern, writerSpread2, semmDiscovery);
			logModelMeanParamters(writerSpread2, semmDiscovery);
		}
		reader.close();
		closeLogger(writerAfterLoc);
		closeLogger(writerSpread1);
		closeLogger(writerSpread2);
		semmDiscovery.finish();
	}
	
	public void mineTwoStepSpreadPatternsSparse(int nrIter) throws FileNotFoundException {
		SemmDiscovery semmDiscovery = new SemmDiscovery(searchParameters, table, targetIdxs);
		Scanner reader = new Scanner(System.in); // Reading from System.in
		// start logger.
		PrintWriter writerAfterLoc = initLogger(pathToLogFile.replace(".m", "BeforeUpdate1.m"));
		PrintWriter writerSpread1 = initLogger(pathToLogFile.replace(".m", "AfterUpdate1.m"));
		PrintWriter writerSpread2 = initLogger(pathToLogFile.replace(".m", "AfterUpdate2.m"));
		
		for (int i = 0; i < nrIter; i++) {
			semmDiscovery.mineLocationPatterns();
			Subgroup selectedPattern = semmDiscovery.getRankKPattern(1);
			this.selectedPatterns.add(selectedPattern);
			logPattern(selectedPattern, writerAfterLoc, semmDiscovery);
			logModelMeanParamters(writerAfterLoc, semmDiscovery);
			semmDiscovery.updateBackgroundDistribution(selectedPatterns);			
			semmDiscovery.findWeightVectorForLocationPatternSparse(selectedPattern);

			this.selectedWs.add(semmDiscovery.getW(selectedPattern.getID()));
			logPattern(selectedPattern, writerSpread1, semmDiscovery);
			logModelMeanParamters(writerSpread1, semmDiscovery);
			semmDiscovery.updateBackgroundDistribution(selectedPatterns, selectedWs);
			semmDiscovery.findWeightVectorForLocationPatternSparse(selectedPattern);

			logPattern(selectedPattern, writerSpread2, semmDiscovery);
			logModelMeanParamters(writerSpread2, semmDiscovery);
		}
		reader.close();
		closeLogger(writerAfterLoc);
		closeLogger(writerSpread1);
		closeLogger(writerSpread2);
		semmDiscovery.finish();
	}	

	public void mineLocationPatterns(int nrIter) throws FileNotFoundException {
		SemmDiscovery semmDiscovery = new SemmDiscovery(searchParameters, table, targetIdxs);
		// start logger.
		PrintWriter writerBefore = initLogger(pathToLogFile.replace(".m", "Before.m"));
		PrintWriter writerAfter = initLogger(pathToLogFile.replace(".m", "After.m"));		
		
		for (int i = 0; i < nrIter; i++) {			
			semmDiscovery.mineLocationPatterns();
			Subgroup selectedSubgroup = semmDiscovery.getRankKPattern(1);
			this.selectedPatterns.add(selectedSubgroup);
			logPattern(selectedSubgroup, writerBefore, semmDiscovery);
			logModelMeanParamters(writerBefore, semmDiscovery);
			semmDiscovery.updateBackgroundDistribution(selectedPatterns);
			logPattern(selectedSubgroup, writerAfter, semmDiscovery);
			logModelMeanParamters(writerAfter, semmDiscovery);
		}
		closeLogger(writerBefore);
		closeLogger(writerAfter);
		semmDiscovery.finish();
	}		

	private PrintWriter initLogger(String pathToLogFile) throws FileNotFoundException {
		PrintWriter writer = new PrintWriter(new FileOutputStream(pathToLogFile, false));
		writer.println(
				"pall = struct('id', '', 'iter', 0, 'desc', '', 'size', 0, 'w', [], 'SIs', [], 'idxs', [], 'targetCol', [], 'v', 0.);");
		writer.println("eall = cell(0);");
		return writer;
	}

	private void closeLogger(PrintWriter writer) {
		writer.println("pall(1) = [];");
		writer.println("clear c;");
		writer.println("clear e;");
		writer.println("clear p;");
		writer.close();
	}
	
	private void logPattern(Subgroup theSubgroup, PrintWriter writer, SemmDiscovery semmDiscovery) {
		BitSet memebers = theSubgroup.getMembers();
		int[] idxs = new int[memebers.cardinality()];
		int idx = 0;
		for (int i = memebers.nextSetBit(0); i != -1; i = memebers.nextSetBit(i + 1)) {
			idxs[idx] = this.supportRowIdxs[i];
			idx++;
		}
		writer.println("p.id = " + theSubgroup.getID() + ";");
		writer.println("p.iter = " + semmDiscovery.getIteration() + ";");
		writer.println("p.desc = '" + theSubgroup.getConditions().toString().replace("'", "") + "';");
		writer.println("p.size = " + theSubgroup.getCoverage() + ";");
		writer.println("p.w = " + Arrays.toString(semmDiscovery.getW(theSubgroup.getID())) + ";");
		writer.println("p.SIs = " + Arrays.toString(semmDiscovery.getAttribueSIs().get(theSubgroup.getID())) + ";");
		writer.println("p.idxs = " + Arrays.toString(idxs) + " + 1;");
		writer.println("p.targetCol = " + Arrays.toString(semmDiscovery.getTargetIdxs()) + "+ 1;");
		writer.println(String.format("p.v = %s;", Double.toString(theSubgroup.getMeasureValue())));
		writer.println(String.format("pall(end+1) = p;"));
	}

	private void logModelMeanParamters(PrintWriter writer, SemmDiscovery semmDiscovery) {
		Object[] para = semmDiscovery.getModelMeanAndVar();
		para = (Object[]) para[0];
		String[] varNames = (String[]) para[0];
		Object[] classes = (Object[]) para[1];
		writer.println("e = struct('mu', [], 'sigma', [], 'inds', []);");
		int nrClasses = classes.length;
		
		for (int i = 0; i < nrClasses; i++) {
			Object[] c = (Object[]) classes[i];
			for (int j = 0; j < varNames.length; j++) {
				double[] values = (double[]) c[j];
				writer.println("c." + varNames[j] + " = " + Arrays.toString(values) + ";"); 
			}
			writer.println("c.sigma = reshape(c.sigma, length(c.mu), length(c.mu))';");
			writer.println("e(end+1, 1) = c;");
		}
		writer.println("e(1) = [];");
		writer.println("eall{end+1} = e;");

	}

	private void printTableInfo() {
		System.out.println(String.format("Table '%s' has %d columns and %d rows.", table.getName(),
				table.getNrColumns(), table.getNrRows()));
	}

}
