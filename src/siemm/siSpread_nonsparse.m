function [w, v, attributeSI] = siSpread_nonsparse(inds, target, sallObject)    
    flag = false;
%     save;
    attributeSI = [];
	% Because the starting index differ 1 between java and matlab.
    inds = inds + 1;
    % convert java input into arry of objects. 
    target = cell2mat(target');
    sall = constructEquiClasses(sallObject);
%     save('workspace.mat', 'inds', 'target', 'sall');
    % compute extension of the target data.
    ext = target(inds,:);
    [n, k] = size(ext);          
    % compute background distribution parameters
    ai = {};
    diff = [];
    avgS = zeros(k,k);
    card = [];
    for s = sall
        int = intersect(s.inds, inds);
        if ~isempty(int)
            card(end+1,1) = length(int);                                    
            ai{end+1,1} = s.S/n;            
            avgS = avgS + card(end,1)*ai{end, 1};
            diff = [diff; target(int,:) - repmat(s.mu', card(end, 1), 1)];
        end
    end
    avgS = avgS/n;
    delta = cell(size(ai));
    for i = 1:length(ai)
        delta{i,1} = ai{i,1} - avgS;
    end       
    
%     mean = 0.07483624275358453;
%     var = 0.0014092675513913608;
%     x = (mean - 20*var) : 1e-4 : (mean + 10*var);
%     vs = zeros(280,1);
%     for i = 1:length(x)
%         w2 = x(i);
%         w = [sqrt(1-w2^2), w2]';
%         vs(i) = sicSpread(w, n, diff, card, delta, avgS);
%     end
%     plot(x, vs);
%     hold on;
%     w = [sqrt(1-mean^2), mean]';
%     plot(mean, sicSpread(w, n, diff, card, delta, avgS), 'ro');
    
    
    
    manifold = spherefactory(k,1);
    problem.M = manifold;
        
    % Define the problem cost function
    problem.cost = @(w) -sicSpread(w, n, diff, card, delta, avgS);
    problem.grad = @(w) -gradient(w, n, diff, card, delta, avgS);
    
    % Numerically check gradient consistency (optional).
%     figure;
%     checkgradient(problem);
%     
    % Solve.
    options.verbosity = 0;
    [w, ~, ~, ~] = steepestdescent(problem, [], options);
    v = sicSpread(w, n, diff, card, delta, avgS);
    
    attributeSI = zeros(k ,1);
    for i = 1:k 
        e = zeros(k,1); 
        e(i) = 1;
        attributeSI(i, 1) = sicSpread(e, n, diff, card, delta, avgS);
    end 

    function value = sicSpread(w,n, diff, card, delta, avgS)
        abar = w'*avgS*w;
        delta2Sum = 0.;
        for j = 1:length(delta)
           delta2Sum = delta2Sum + card(j,1)*(w'*delta{j,1}*w)^2; 
        end
        alpha = abar + 2*delta2Sum/(n*abar);
        beta = delta2Sum/abar;
        tws = norm(diff*w)^2/n;
        value = n/2*log(2) + gammaln(n/2) + alpha - (n/2-1)*log((tws-beta)/alpha) + (tws-beta)/(2*alpha);
    end

    function g = gradient(w, n, diff, card, delta, avgS)        
        pDelta = {};
        pBeta = zeros(size(w));
        
        abar = w'*avgS*w;
        pAbar = 2*avgS*w;        
        delta2Sum = 0.;
        for j = 1:length(delta)
            pDelta{end+1, 1} = 2*delta{j}*w;  
            deltaj = w'*delta{j}*w;
            delta2Sum = delta2Sum + card(j)*deltaj^2;             
            pBeta = pBeta + (2*card(j)*deltaj*abar*pDelta{end,1} - card(j)*deltaj^2*pAbar)/(abar^2);            
        end
        pAlpha = pAbar + 2/n*pBeta;
        alpha = abar + 2*delta2Sum/(n*abar);
        beta = delta2Sum/abar;
        pTws = 2/n * (diff'*diff)*w;
        tws = norm(diff*w)^2/n;
        pc = ((pTws-pBeta)*alpha - pAlpha*(tws - beta))/(alpha^2);
        g = pAlpha - (n/2 - 1) * alpha/(tws - beta) * pc + 1./2*pc;
    end
end