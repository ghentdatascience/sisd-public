addpath ../plotters;
addpath ../../germanSocioEconomics;

% prepare data.
fileName ='vote_demo_work_2009.csv';
d = importdata(fileName,',', 1);
attributes = d.colheaders;
X = d.data;
[data, ~] = loadDataAndConfig();

% prepare figure
f = figure('Units','centimeters',...
        'OuterPosition',[3 3 10 5],'Resize','on', 'PaperOrientation',...
        'landscape','PaperType','A4','papersize', [10, 5], ...
        'Renderer','painters','Visible', 'off');
lm = 0.01;
rm = 0.01;
chm = 0.04;
width = (1-lm-rm-chm)/2;
bm = 0.03;
tm = 0.07;
height = 1-bm-tm;


spreadPatternsBeforeUpdate1;

axes('Position',[lm bm width height],'FontSize',10.0);
hold on;
p = pall(2);
% inds = [326, 81, 225, p.idxs(randi(length(p.idxs),1,12))];
inds = p.idxs(randi(length(p.idxs),1,15));
    
% p.idxs;
germanMapPatterns(data.xCoords, data.yCoords, p.idxs, inds, data.values(2, inds), {data.boundaryX, data.boundaryY}, 'figure', f);
% t = title('(a)');
% set(t,'interpreter','none');
ylim([-1.8, 2.3]);
text(-2, 2.3, '(a)', 'FontSize', 15);
set(gca, 'XTickLabel','', 'YTickLabel','', 'XTick',[],'YTick',[],'XColor','w','YColor','w', 'layer','top');
 
axes('Position',[lm+width+chm bm width height],'FontSize',10.0);
hold on;
p = pall(3);
inds = p.idxs(randi(length(p.idxs),15,1));
% inds = [107    83    81   225    16   119      32     141    58     5   212   123   145];
germanMapPatterns(data.xCoords, data.yCoords, p.idxs, inds, data.values(2, inds), {data.boundaryX, data.boundaryY}, 'figure', f);
% t = title('(b)');
% set(t,'interpreter','none');
ylim([-1.8, 2.3]);
text(-2, 2.3, '(b)', 'FontSize', 15);
set(gca, 'XTickLabel','', 'YTickLabel','', 'XTick',[],'YTick',[],'XColor','w','YColor','w', 'layer','top');

set(gcf, 'Color', 'w');
hold off;


print(f, '-dpdf', '-fillpage', '2.pdf');
clearvars p pall eall title d patterns fileName;


rmpath ../../germanSocioEconomics;
rmpath ../plotters;