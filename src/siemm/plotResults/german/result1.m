addpath ../plotters;
addpath ../../germanSocioEconomics;

% prepare data.
fileName ='vote_demo_work_2009.csv';
d = importdata(fileName,',', 1);
attributes = d.colheaders;
X = d.data;
[data, ~] = loadDataAndConfig();

% prepare figure
f = figure('Units','centimeters',...
        'OuterPosition',[3 3 60 13],'Resize','on', 'PaperOrientation',...
        'landscape','PaperType','A4','papersize', [35, 8], ...
        'Renderer','painters','Visible', 'off');
lm = 0.03;
rm = 0.03;
chm = 0.08;
width = (1-lm-rm-3*chm)/4;
bm = 0.13;
tm = 0.07;
height = 1-bm-tm;

patternId = 1;

spreadPatternsAfterUpdate1
eallAfter = eall;
spreadPatternsBeforeUpdate1;


axes('Position',[0.01 0.08 width+0.045 height+0.05],'FontSize',20.0);
hold on;
p = pall(patternId);
% einds = [197, 296, 248, 350, 347, 368, 373, 378, 394, 383, 341, 352, 380, 366, 407];
inds = [];
germanMapPatterns(data.xCoords, data.yCoords, p.idxs, inds, data.values(2, inds), ...
    {data.boundaryX, data.boundaryY}, 'figure', f, 'markerSize', 30);
% xrange = xlim;
% yrange = ylim;
% text(xrange(1), yrange(2)-.5, '(a)', 'FontSize', 25);

ylim([-1.8, 2.3]);
text(-2, 2.3, '(a)', 'FontSize', 25);
set(gca, 'XTickLabel','', 'YTickLabel','', 'XTick',[],'YTick',[],'XColor','w','YColor','w', 'layer','top');
 
% box plot before
axes('Position',[lm+1*(width+chm) bm width+0.03 height],'FontSize',20.0);
hold on;
p = pall(patternId);
[~, sortedAttrIdxs] = sort(p.SIs, 'descend');
boxPlotBgDistAndPattern(X, p.idxs, eall{p.iter}, eallAfter{p.iter}, ...
sortedAttrIdxs, attributes(sortedAttrIdxs), 'figure', f);
xlim([-5.2, 52]);
plot([-5.18, -5.18], [0, 8],'w-', 'LineWidth', 5);
% set(gca, 'TickLength', [0.0100 0.1])
xrange = xlim;
yrange = ylim;
text(xrange(1)+2, yrange(1), '(b)', 'FontSize', 25);
set(gca, 'TickDir', 'out');

% % box plot after update location
% spreadPatternsAfterUpdate1
% axes('Position',[lm+2*(width+chm)+0.03 bm width height],'FontSize',20.0);
% hold on;
% p = pall(1);
% % [~, sortedAttrIdxs] = sort(p.SIs, 'descend');
% boxPlotBgDistAndPattern(X, p.idxs, eall{p.iter}, sortedAttrIdxs, attributes(sortedAttrIdxs), 'figure', f);
% xlim([-5, 52]);
% t = title('(c)');
% set(t,'interpreter','none');

%contour plot
spreadPatternsAfterUpdate1;
axes('Position',[lm+2*(width+chm)-0.01 bm width+0.02 height],'FontSize',20.0);
hold on;
p = pall(patternId);
attrIds = find(p.w ~= 0);
contourBgDistAndPattern(X, p, eall{p.iter}, attrIds, attributes(attrIds), p.w(attrIds), 'figure', f, 'markerSize', 15);    
xrange = xlim;
yrange = ylim;
text(xrange(1)+2, yrange(2), '(c)', 'FontSize', 25);
set(gca, 'TickDir', 'out');

% cdf plot
spreadPatternsAfterUpdate1;
axes('Position',[lm+3*(width+chm)-0.02 bm width+0.02 height],'FontSize',20.0);
p = pall(patternId);
attrIds = find(p.w ~= 0);
plotCDF(X(:, attrIds), p.idxs, eall{p.iter}, attrIds, p.w(attrIds), 'figure', f);
xlabel('Xw', 'FontSize', 25);
ylabel('Probability', 'FontSize', 25);
xlim([20, 45]);
title('');
xrange = xlim;
yrange = ylim;
text(xrange(1)+1, yrange(2), '(d)', 'FontSize', 25);
set(gca, 'TickDir', 'out');
box off;

set(gcf,'color','w');
hold off;

print(f, '-dpdf', '-fillpage', '1.pdf');
clearvars p pall eall title d patterns fileName;


rmpath ../../germanSocioEconomics;
rmpath ../plotters;