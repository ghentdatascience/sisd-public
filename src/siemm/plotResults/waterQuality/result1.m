addpath ../plotters;
addpath ../../waterQuality;

% prepare data.
fileName ='waterQuality.csv';
d = importdata(fileName,',', 1);
attributes = d.colheaders;
X = d.data;
patternId = 1;

% prepare figure
f = figure('Units','centimeters',...
        'OuterPosition',[3 3 60 13],'Resize','on', 'PaperOrientation',...
        'landscape','PaperType','A4','papersize', [35, 8], ...
        'Renderer','painters','Visible', 'off');
lm = 0.04;
rm = 0.03;
chm = 0.035;
width = (1-lm-rm-3*chm)/4;
bm = 0.08;
tm = 0.08;
height = 1-bm-tm;

spreadPatternsAfterUpdate1;
eallAfter = eall;
spreadPatternsBeforeUpdate1;
axes('Position',[lm bm+0.05 width height],'FontSize',25.0);
hold on;
p = pall(patternId);
[~, sortedAttrIdxs] = sort(p.SIs, 'descend');
weightsIdxs = sortedAttrIdxs;
sortedAttrIdxs = sortedAttrIdxs(1:5);

boxPlotBgDistAndPattern(X, p.idxs, eall{p.iter}, eallAfter{p.iter}, ...
    sortedAttrIdxs, attributes(sortedAttrIdxs),'figure', f, 'markerSize', 15);
xlim([-5, 21]);
ylim([0,7.5]);
plot([-4.97 -4.97], [0, 7.5],'w-', 'LineWidth', 5);
xrange = xlim;
yrange = ylim;
text(xrange(1)+1, yrange(1), '(a)', 'FontSize', 25);
% title('(a)');
clearvars p pall eall;

axes('Position',[lm+1*(width+chm) bm+0.05 width height-0.06],'FontSize',25.0);
hold on;
spreadPatternsAfterUpdate1;
p = pall(patternId);
[~, attrIds] = sort(abs(p.w), 'descend');
attrIds = attrIds(1:2);
w = p.w(attrIds)/norm(p.w(attrIds));
contourBgDistAndPattern(X, p, eall{p.iter}, attrIds, attributes(attrIds), ...
    p.w(attrIds)/2.5, 'figure', f, 'markerSize', 15);    
% f = barWeights(p.w(weightsIdxs), attributes(weightsIdxs), 'figure',f);
% set(gca, 'XTickLabelRotation',50);
xlim([-1, 8]);
ylim([-2, 20]);

xrange = xlim;
yrange = ylim;
text(xrange(1)+0.2, yrange(2), '(b)', 'FontSize', 25);
set(gca, 'TickDir', 'out');

% plot cdf
spreadPatternsAfterUpdate1;
axes('Position',[lm+2*(width+chm) bm+0.05 width height-0.05],'FontSize',25.0);
hold on;
p = pall(patternId);
[~, attrIds] = sort(abs(p.w), 'descend');
attrIds = attrIds(1:2);
plotCDF(X(:, attrIds), p.idxs, eall{p.iter}, attrIds, p.w(attrIds), 'figure', f, 'legendPos', 'west');
xlim([-6,2.5]);
xrange = xlim;
yrange = ylim;
text(xrange(1)+0.2, yrange(2), '(c)', 'FontSize', 25);
set(gca, 'TickDir', 'out');
xlabel('Xw', 'FontSize', 25);
ylabel('Probability', 'FontSize', 25);
title('');

% plot weights
spreadPatternsAfterUpdate1;
axes('Position',[lm+3*(width+chm) bm+0.12 width height-0.12],'FontSize',25.0);
hold on;
p = pall(patternId);
f = barWeights(p.w, attributes(1:16), 'figure',f);
set(gca, 'XTickLabelRotation',50);
xlim([0,16.5]);
% ylim([-0.75, 0.75]);

xrange = xlim;
yrange = ylim;
text(xrange(1)+0.4, yrange(2), '(d)', 'FontSize', 25);
set(gca, 'TickDir', 'out');

print(f, '-dpdf', '-fillpage', '1.pdf');
clearvars p pall eall title d patterns fileName;


rmpath ../../waterQuality;
rmpath ../plotters;