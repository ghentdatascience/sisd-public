package siemm.plotResults.synthetic;

import java.io.*;
import java.util.*;

import nl.liacs.subdisc.*;
import siemm.*;

public class result4 {
	public static void main(String[] args) throws FileNotFoundException {
		PrintWriter writer = new PrintWriter(new FileOutputStream("src/siemm/plotResults/synthetic/result4.txt", false));
		
		Table table = new DataLoaderTXT(new File("src/siemm/synthetic/spreadData.csv")).getTable();
		table.update();
		int[] targetIdxs = {0,1};
		SearchParameters searchParameters = configSearchParameters(table, targetIdxs);
		for (int i = 1; i <= 3; i++) {
			SemmDiscovery semmDiscovery = new SemmDiscovery(searchParameters, table, targetIdxs);		
			semmDiscovery.mineLocationPatterns();
			Subgroup selectedPattern = semmDiscovery.getRankKPattern(i);
			List<Subgroup> selectedPatterns = new ArrayList<>();
			selectedPatterns.add(selectedPattern);
			semmDiscovery.updateBackgroundDistribution(selectedPatterns);
			semmDiscovery.findWeightVectorForLocationPatternNonSparse(selectedPattern);
			
			logMeasureOfSubgroups(writer, selectedPatterns, semmDiscovery);
			List<double []> ws = new ArrayList<>();
			List<Double> vs = new ArrayList<>();
			for (int j = 0; j < 100; j++) {
				Triplet t = semmDiscovery.evaluateCandidate(selectedPattern);
				for (int k =0; k < t.w.length; k++) {
					t.w[k] = Math.abs(t.w[k]);
				}
				ws.add(t.w);
				vs.add((double)t.v);
			}
			
			writer.println("mean of w: \n" + Arrays.toString(computeMeanOfMatrix(ws)));
			writer.println("variance of w: \n" + Arrays.toString(computeVarianceMatrix(ws)));
			writer.println("mean of v: \n" + computeMeanOfVector(vs));
			writer.println("variance of v: \n" + computeVarianceOfVector(vs));
			writer.println("\n\n");
			
			semmDiscovery.finish();
		}
				
		writer.close();
		
		
	}
	
	private static double[] computeMeanOfMatrix(List<double []> matrix) {
		int n = matrix.size();
		int d = matrix.get(0).length;
		double[] result = new double[d];
		for (int j = 0; j < d; j++) {
			result[j] = 0.;
		}
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < d; j++) {
				result[j] += matrix.get(i)[j];
			}
		}		
		for (int j = 0; j < d; j++) {
			result[j] /= n;
		}			
		
		return result;
	}
	
	private static double[] computeVarianceMatrix(List<double []> matrix) {
		int n = matrix.size();
		int d = matrix.get(0).length;
		double[] meanVec = computeMeanOfMatrix(matrix);
		double[] result = new double[d];
		for (int i = 0; i < d; i++) {
			result[i] = 0.;
		}	
		
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < d; j++) {
				result[j] += Math.pow((matrix.get(i)[j] - meanVec[j]), 2);
			}
		}
		
		for (int j = 0; j < d; j++) {
			result[j] /= n;
		}
		
		
		return result;
	} 
	
	private static double computeMeanOfVector(List<Double> vector) {
		int d = vector.size();
		double result = 0.;
		for (Double v : vector) {
			result += v;
		}
		result /= d;
		return result;
	}
	private static double computeVarianceOfVector(List<Double> vector) {
		int d = vector.size();
		double mean = computeMeanOfVector(vector);
		double result = 0.;
		for (Double v : vector) {
			result += Math.pow(v - mean, 2.0);
		}
		result /= d;
		return result;
	}
	
	private static void logMeasureOfSubgroups(PrintWriter writer, Iterable<Subgroup> subgroups, SemmDiscovery semmDiscovery) {
		for (Subgroup subgroup : subgroups) {
			Triplet measurs = semmDiscovery.evaluateCandidate(subgroup);
			StringBuffer sb = new StringBuffer(200);
			sb.append("s.id = ");
			sb.append(subgroup.getID());
			sb.append(" candidate ");
			sb.append(subgroup.getConditions());
			sb.append(" size: ");
			sb.append(subgroup.getCoverage());
			sb.append(" si: ");
			sb.append(measurs.v);
			writer.println(sb.toString());
		}
		
	}

	private static SearchParameters configSearchParameters(Table table, int[] targetIdxs) {
		// configure search parameter
		SearchParameters searchParameters = new SearchParameters();
		searchParameters.setTargetConcept(configTargetConcept(table, targetIdxs));		
		
		searchParameters.setSearchDepth(4);
		searchParameters.setMinimumCoverage(10);
		searchParameters.setQualityMeasureMinimum((float) -10);
		searchParameters.setMaximumCoverageFraction((float) 0.8);
		searchParameters.setMaximumSubgroups(150);
		searchParameters.setMaximumTime((float) 5);		

		searchParameters.setSearchStrategy("beam");
		searchParameters.setSearchStrategyWidth(40);
		searchParameters.setNominalSets(false);
		searchParameters.setNumericStrategy("bins");
		searchParameters.setNumericOperators("<html>&#8804;, &#8805;</html>");
		searchParameters.setNrBins(5);
		
		return searchParameters;
	}	
	
	private static TargetConcept configTargetConcept(Table table, int[] targetIdxs) {
		TargetConcept targetConcept = new TargetConcept();
		targetConcept.setTargetType("multi-label");
		List<Column> multiTargets = new ArrayList<>();
		for (int idx : targetIdxs) {
			multiTargets.add(table.getColumn(idx));
		}		
		targetConcept.setMultiTargets(multiTargets);
		return targetConcept;
	}
}
