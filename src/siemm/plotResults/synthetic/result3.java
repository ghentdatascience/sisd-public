package siemm.plotResults.synthetic;

import java.io.*;
import java.util.*;

import matlabcontrol.*;
import nl.liacs.subdisc.*;
import siemm.*;

public class result3 {
	public static void main(String[] args) throws FileNotFoundException {
		PrintWriter writer = new PrintWriter(new FileOutputStream("src/siemm/plotResults/synthetic/result3.txt", false));
		PrintWriter writer2 = new PrintWriter(new FileOutputStream("src/siemm/plotResults/synthetic/result3script.m", false));
		try {
			MatlabProxyFactory factory = new MatlabProxyFactory();					
			MatlabProxy proxy = factory.getProxy();
			proxy.eval("addpath src/siemm/plotResults/synthetic/");																
			double baseLine = computeExpectedMeanOfSI();
			List<Condition> conditions = computeTopThreeConditions();
			System.out.println(conditions);
			double p = 0.0;
			
			double currentSI = Double.MAX_VALUE;
			
			writer.println("Base Line:" + baseLine);
			List<List<Double>> result = new ArrayList<>();
			do {
				proxy.feval("generateNoisyData", p);							
				Table table = new DataLoaderTXT(new File("src/siemm/plotResults/synthetic/spreadData.csv")).getTable();
				int[] targetIdxs = {0,1};
				SearchParameters searchParameters = configSearchParameters(table, targetIdxs);		
				SemmDiscovery semmDiscovery = new SemmDiscovery(searchParameters, table, targetIdxs);
				List<Subgroup> subgroups = new ArrayList<>();
				
				writer.println("p = " + p);
	
				semmDiscovery.mineLocationPatterns();
				subgroups.add(semmDiscovery.getRankKPattern(1));
				List<Double> values = new ArrayList<>();
				values.add(p);
				values.add((double) semmDiscovery.evaluateCandidate(table.getColumn(2).evaluate(conditions.get(0)), 1).v);
				values.add((double) semmDiscovery.evaluateCandidate(table.getColumn(4).evaluate(conditions.get(1)), 1).v);
				values.add((double) semmDiscovery.evaluateCandidate(table.getColumn(3).evaluate(conditions.get(2)), 1).v);
				result.add(values);				
				currentSI = values.get(1);
				currentSI = (currentSI < values.get(2))?values.get(2): currentSI; 
				currentSI = (currentSI < values.get(3))?values.get(3): currentSI;
				writer.println("Iter1: ");				
				logMeasureOfSubgroups(writer, semmDiscovery.getRankKPattern(1), semmDiscovery);
				logMeasureOfSubgroups(writer, semmDiscovery.getRankKPattern(2), semmDiscovery);
				logMeasureOfSubgroups(writer, semmDiscovery.getRankKPattern(3), semmDiscovery);
				
				
				semmDiscovery.updateBackgroundDistribution(subgroups);								
				semmDiscovery.mineLocationPatterns();
				subgroups.add(semmDiscovery.getRankKPattern(1));
				writer.println("Iter2: ");				
				logMeasureOfSubgroups(writer, semmDiscovery.getRankKPattern(1), semmDiscovery);
				logMeasureOfSubgroups(writer, semmDiscovery.getRankKPattern(2), semmDiscovery);
				logMeasureOfSubgroups(writer, semmDiscovery.getRankKPattern(3), semmDiscovery);
				
				semmDiscovery.updateBackgroundDistribution(subgroups);				
				semmDiscovery.mineLocationPatterns();
				subgroups.add(semmDiscovery.getRankKPattern(1));
				writer.println("Iter3: ");				
				logMeasureOfSubgroups(writer, semmDiscovery.getRankKPattern(1), semmDiscovery);
				logMeasureOfSubgroups(writer, semmDiscovery.getRankKPattern(2), semmDiscovery);
				logMeasureOfSubgroups(writer, semmDiscovery.getRankKPattern(3), semmDiscovery);
					
				semmDiscovery.finish();	
				p += 0.01;
				System.out.println(p + " " + currentSI + " " + baseLine);
//			} while (currentSI > baseLine && p < );
			} while (p < 0.35);
			proxy.eval("rmpath src/siemm/plotResults/synthetic/");
			proxy.exit();
			writer2.print("A = [");
			for (List<Double> row : result) {
				writer2.print(row.toString() + ";");
			}
			writer2.println("];");
			writer.close();
			writer2.close();
			

		} catch (MatlabInvocationException | MatlabConnectionException e) {
			e.printStackTrace();
		}					
	}
	
	private static void logMeasureOfSubgroups(PrintWriter writer, Subgroup subgroup, SemmDiscovery semmDiscovery) {
//		for (Subgroup subgroup : subgroups) {
			Triplet measurs = semmDiscovery.evaluateCandidate(subgroup);
			StringBuffer sb = new StringBuffer(200);
			sb.append("s.id = ");
			sb.append(subgroup.getID());
			sb.append(" candidate ");
			sb.append(subgroup.getConditions());
			sb.append(" size: ");
			sb.append(subgroup.getCoverage());
			sb.append(" si: ");
			sb.append(measurs.v);
			writer.println(sb.toString());
//		}
		
	}
	
	private static double computeExpectedMeanOfSI() {
		double result = 0.;
		int nrSamples = 100;
		Table origionalTable = new DataLoaderTXT(new File("src/siemm/plotResults/synthetic/originalSpreadData.csv")).getTable();
		int[] targetIdxs = {0,1};
		SearchParameters searchParameters = configSearchParameters(origionalTable, targetIdxs);		
		SemmDiscovery semmDiscovery = new SemmDiscovery(searchParameters, origionalTable, targetIdxs);
		semmDiscovery.mineLocationPatterns();
		for (int i = 0; i < nrSamples; i++) {			
			float a = semmDiscovery.evaluateCandidate(origionalTable.getRandomSubgroup(40).getMembers(), 1).v;
			result += a;			
		}
		semmDiscovery.finish();
		return result/nrSamples;
	}
	
	private static List<Condition> computeTopThreeConditions() {
		List<Condition> result = new ArrayList<>();
		Table origionalTable = new DataLoaderTXT(new File("src/siemm/plotResults/synthetic/originalSpreadData.csv")).getTable();
		int[] targetIdxs = {0,1};
		SearchParameters searchParameters = configSearchParameters(origionalTable, targetIdxs);		
		SemmDiscovery semmDiscovery = new SemmDiscovery(searchParameters, origionalTable, targetIdxs);
		semmDiscovery.mineLocationPatterns();
		result.add(semmDiscovery.getRankKPattern(1).getConditions().get(0));
		result.add(semmDiscovery.getRankKPattern(2).getConditions().get(0));
		result.add(semmDiscovery.getRankKPattern(3).getConditions().get(0));
		semmDiscovery.finish();
		return result;
	}

	private static SearchParameters configSearchParameters(Table table, int[] targetIdxs) {
		// configure search parameter
		SearchParameters searchParameters = new SearchParameters();
		searchParameters.setTargetConcept(configTargetConcept(table, targetIdxs));		
		
		searchParameters.setSearchDepth(4);
		searchParameters.setMinimumCoverage(10);
		searchParameters.setQualityMeasureMinimum((float) -10);
		searchParameters.setMaximumCoverageFraction((float) 0.8);
		searchParameters.setMaximumSubgroups(150);
		searchParameters.setMaximumTime((float) 5);		

		searchParameters.setSearchStrategy("beam");
		searchParameters.setSearchStrategyWidth(40);
		searchParameters.setNominalSets(false);
		searchParameters.setNumericStrategy("bins");
		searchParameters.setNumericOperators("<html>&#8804;, &#8805;</html>");
		searchParameters.setNrBins(5);
		
		return searchParameters;
	}	
	
	private static TargetConcept configTargetConcept(Table table, int[] targetIdxs) {
		TargetConcept targetConcept = new TargetConcept();
		targetConcept.setTargetType("multi-label");
		List<Column> multiTargets = new ArrayList<>();
		for (int idx : targetIdxs) {
			multiTargets.add(table.getColumn(idx));
		}		
		targetConcept.setMultiTargets(multiTargets);
		return targetConcept;
	}
}
