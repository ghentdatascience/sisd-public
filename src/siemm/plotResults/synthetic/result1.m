addpath ../plotters;
addpath ../../synthetic;

% prepare data.
fileName = 'originalSpreadData.csv';
d = importdata(fileName,',', 1);
attributes = d.colheaders;
X = d.data;
patterns = cell(3,1);
spreadPatternsAfterUpdate1
for i = 1:length(patterns)
    patterns{i} = pall(i).idxs;
end

% prepare figure
f = figure('Units','centimeters',...
        'OuterPosition',[3 3 60 13],'Resize','on', 'PaperOrientation',...
        'landscape','PaperType','A4','papersize', [35, 8], ...
        'Renderer','painters','Visible', 'off');
lm = 0.03;
rm = 0.03;
chm = 0.035;
width = (1-lm-rm-3*chm)/4;
bm = 0.13;
tm = 0.05;
height = 1-bm-tm;

% 1. plot data
spreadPatternsAfterUpdate1
axes('Position',[lm bm width height],'FontSize',28.0);
hold on;
scatterDataAndPatterns(X, patterns, {'Attribute 1', 'Attribute 2'}, 'figure', f);
ylim([-4,4]);
text(-3.5,3.8, '(a)', 'FontSize', 28);   
set(gca, 'TickDir', 'out');

% 2. plot scatter for each pattern
spreadPatternsAfterUpdate1
safeColor;
colors = color.collection;
titles = {'(b)', '(c)', '(d)'};
for i = 1:3
    axes('Position',[lm+i*(width+chm) bm width height],'FontSize',28.0);
    hold on;
    p = pall(i);
    scatterDataAndPatterns(X, {p.idxs}, {'Attribute 1', 'Attribute 2'}, 'w', p.w, 'colors', colors(i), 'figure', f);
    ylim([-4,4]);
    % t = title(titles{i});
    % set(t,'interpreter','none');
    text(-3.5,3.8, titles{i}, 'FontSize', 28);   
    set(gca, 'TickDir', 'out');
end

print(f, '-dpdf', '-fillpage', '1.pdf');
clearvars p pall eall title d patterns fileName;


rmpath ../../synthetic;
rmpath ../plotters;