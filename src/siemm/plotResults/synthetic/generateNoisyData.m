function generateNoisyData(p)
    randomPointSize = 500;
    patternSize = 40;
    dataSize = randomPointSize + 3*patternSize;
    
    X = csvread('originalSpreadData.csv');
    X = X(2:end, 1:2);
    

    f1 = zeros(dataSize,1);
%     f1(end-3*patternSize+1:end-2*patternSize) = binornd(1, p, patternSize, 1);
    f1(end-3*patternSize+1:end-2*patternSize) = 1;
    f1(end-2*patternSize+1:end-patternSize) = 0;
    f1(end-patternSize+1:end) = 0;
    pos = find(binornd(1, p, dataSize, 1));
    f1(pos) = ~f1(pos);

    f2 = zeros(dataSize,1);
    f2(end-3*patternSize+1:end-2*patternSize) = 0;
%     f2(end-2*patternSize+1:end-patternSize) = binornd(1, p, patternSize, 1);
    f2(end-2*patternSize+1:end-patternSize) = 1;
    f2(end-patternSize+1:end) = 0;
    pos = find(binornd(1, p, dataSize, 1));
    f2(pos) = ~f2(pos);

    f3 = zeros(dataSize,1);
    f3(end-3*patternSize+1:end-2*patternSize) = 0;
    f3(end-2*patternSize+1:end-patternSize) = 0;
%     f3(end-patternSize+1:end) = binornd(1, p, patternSize, 1);
    f3(end-patternSize+1:end) = 1;
    pos = find(binornd(1, p, dataSize, 1));
    f3(pos) = ~f3(pos);

    f4 = randi(2,dataSize,1) - 1;
    f5 = randi(2,dataSize,1) - 1;

    result = [X, f1, f2, f3, f4, f5];
    result = vertcat(1:7, result);
    csvwrite('src/siemm/plotResults/synthetic/spreadData.csv', result);
end



