package siemm.plotResults.synthetic;

import java.io.*;
import java.util.*;

import nl.liacs.subdisc.DataLoaderTXT;
import nl.liacs.subdisc.Table;
import nl.liacs.subdisc.SearchParameters;
import nl.liacs.subdisc.Subgroup;
import nl.liacs.subdisc.SubgroupSet;
import nl.liacs.subdisc.SemmDiscovery;
import nl.liacs.subdisc.TargetConcept;
import nl.liacs.subdisc.Column;

import siemm.*;

public class result2 {
	public static void main(String[] args) throws FileNotFoundException {
		PrintWriter writer = new PrintWriter(new FileOutputStream("src/siemm/plotResults/synthetic/result2.txt", false));
		
		Table table = new DataLoaderTXT(new File("src/siemm/synthetic/spreadData.csv")).getTable();
		table.update();
		int[] targetIdxs = {0,1};
		SearchParameters searchParameters = configSearchParameters(table, targetIdxs);		
		SemmDiscovery semmDiscovery = new SemmDiscovery(searchParameters, table, targetIdxs);
		List<Subgroup> selectedPatterns = new ArrayList<>();
		List<Subgroup> top10Patterns = new ArrayList<>();

		for (int i = 0; i < 4; i++) {
			semmDiscovery.mineLocationPatterns();
			Subgroup selectedPattern = semmDiscovery.getRankKPattern(1);
			selectedPatterns.add(selectedPattern);
			
			if (i == 0) {
				int count = 0;
				SubgroupSet results = semmDiscovery.getResults();
				for (Subgroup subgroup : results) {			
					if (count == 10) {
						break;
					}
					top10Patterns.add(subgroup);
					count++;
				}
			}
			writer.println("\n\n Iteration: " + (i+1) + ":");
			writer.println("Interstingness of patterns found current iteraton:");
			logMeasureOfSubgroups(writer, semmDiscovery.getResults(), semmDiscovery);
			writer.println("\nInterstingness of top 10 patterns in current iteraton:");
			logMeasureOfSubgroups(writer, top10Patterns, semmDiscovery);
			
			semmDiscovery.updateBackgroundDistribution(selectedPatterns);
		}
		writer.close();
		semmDiscovery.finish();
	}
	
	private static void logMeasureOfSubgroups(PrintWriter writer, Iterable<Subgroup> subgroups, SemmDiscovery semmDiscovery) {
		for (Subgroup subgroup : subgroups) {
			Triplet measurs = semmDiscovery.evaluateCandidate(subgroup);
			StringBuffer sb = new StringBuffer(200);
			sb.append("s.id = ");
			sb.append(subgroup.getID());
			sb.append(" candidate ");
			sb.append(subgroup.getConditions());
			sb.append(" size: ");
			sb.append(subgroup.getCoverage());
			sb.append(" si: ");
			sb.append(measurs.v);
			writer.println(sb.toString());
		}
		
	}

	private static SearchParameters configSearchParameters(Table table, int[] targetIdxs) {
		// configure search parameter
		SearchParameters searchParameters = new SearchParameters();
		searchParameters.setTargetConcept(configTargetConcept(table, targetIdxs));		
		
		searchParameters.setSearchDepth(4);
		searchParameters.setMinimumCoverage(10);
		searchParameters.setQualityMeasureMinimum((float) -10);
		searchParameters.setMaximumCoverageFraction((float) 0.8);
		searchParameters.setMaximumSubgroups(150);
		searchParameters.setMaximumTime((float) 5);		

		searchParameters.setSearchStrategy("beam");
		searchParameters.setSearchStrategyWidth(40);
		searchParameters.setNominalSets(false);
		searchParameters.setNumericStrategy("bins");
		searchParameters.setNumericOperators("<html>&#8804;, &#8805;</html>");
		searchParameters.setNrBins(5);
		
		return searchParameters;
	}	
	
	private static TargetConcept configTargetConcept(Table table, int[] targetIdxs) {
		TargetConcept targetConcept = new TargetConcept();
		targetConcept.setTargetType("multi-label");
		List<Column> multiTargets = new ArrayList<>();
		for (int idx : targetIdxs) {
			multiTargets.add(table.getColumn(idx));
		}		
		targetConcept.setMultiTargets(multiTargets);
		return targetConcept;
	}
}
