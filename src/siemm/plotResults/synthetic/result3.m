addpath ../plotters;
addpath ../../synthetic;

result3script;

f = figure('Units','centimeters',...
        'OuterPosition',[3 3 15 8],'Resize','on', 'PaperOrientation',...
        'landscape','PaperType','A4', 'papersize', [4, 2.5], 'Renderer','painters','Visible', 'off');
lm = 0.09;
rm = 0.03;
width = 1-lm-rm;
bm = 0.12;
tm = 0.00;
height = 1-bm-tm;

% 1. plot data
axes('Position',[lm bm width height],'FontSize',8.0);
hold on;
safeColor;
plot(A(:,1),A(:,2), '-', 'color', color.orange.rgb);
hold on;
plot(A(:,1),A(:,3), '-', 'color', color.blue.rgb)
plot(A(:,1),A(:,4), '-.', 'color', color.red.rgb)
ylabel('SI');
xlabel('Distortion');
X = 0:0.01:0.35;
plot(X, ones(size(X))*-0.49305944457650186, 'k-', 'LineWidth', 0.5);

legend({'attr 3 = ''1''', 'attr 5 = ''1''', 'attr 4 = ''1''', 'baseline'}, 'FontSize',8.0);
set(gca, 'TickDir', 'out');
print(f, '-dpdf', '-fillpage', '3.pdf');

rmpath ../../synthetic;
rmpath ../plotters;