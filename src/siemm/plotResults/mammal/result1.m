addpath ../plotters;
addpath ../../mammal;

% prepare data.
% plot synthetic location results
load('tarAttrs.mat')
X = csvread('mammals.txt');
xCoords = X(:, 2);
yCoords = X(:, 1);
X(:, 1:2) = [];
n = size(X, 1);
tarAttrs([13,31,33]) = [];
nrAttrs = 15; 

% prepare figure
f = figure('Units','centimeters',...
        'OuterPosition',[3 3 30 10],'Resize','on', 'PaperOrientation',...
        'landscape','PaperType','A4','papersize', [12, 4], ...
        'Renderer','painters','Visible', 'off');
lm = 0.01;
rm = 0.01;
chm = 0.02;
width = (1-lm-rm-chm)/3;
bm = 0.05;
tm = 0.05;
height = 1-bm-tm;

locationPatternsBefore;

axes('Position',[lm bm width height],'FontSize',20.0);
hold on;
p = pall(1);
% title = sprintf('id: %d, iter: %d, desc: %s, v: %.5f', p.id,p.iter,p.desc, p.v);
mammalMapPatterns(xCoords, yCoords, p.idxs, 'figure', f);
xrange = xlim;
yrange = ylim;
text(xrange(1), yrange(2), '(a)', 'FontSize', 18);
set(gca, 'XTickLabel','', 'YTickLabel','', 'XTick',[],'YTick',[],'XColor','w','YColor','w', 'layer','top');


axes('Position',[lm+1*(width+chm) bm width height],'FontSize',20.0);
hold on;
p = pall(2);
% title = sprintf('id: %d, iter: %d, desc: %s, v: %.5f', p.id,p.iter,p.desc, p.v);
mammalMapPatterns(xCoords, yCoords, p.idxs, 'figure', f);
xrange = xlim;
yrange = ylim;
text(xrange(1), yrange(2), '(b)', 'FontSize', 18);
set(gca, 'XTickLabel','', 'YTickLabel','', 'XTick',[],'YTick',[],'XColor','w','YColor','w', 'layer','top');

axes('Position',[lm+2*(width+chm) bm width height],'FontSize',20.0);
hold on;
p = pall(3);
mammalMapPatterns(xCoords, yCoords, p.idxs, 'figure', f);
xrange = xlim;
yrange = ylim;
text(xrange(1), yrange(2), '(c)', 'FontSize', 18);
set(gca, 'XTickLabel','', 'YTickLabel','', 'XTick',[],'YTick',[],'XColor','w','YColor','w', 'layer','top');


set(gcf, 'Color', 'w');
hold off;


print(f, '-dpdf', '-fillpage', '1.pdf');
clearvars p pall eall title d patterns fileName;


rmpath ../../mammal;
rmpath ../plotters;