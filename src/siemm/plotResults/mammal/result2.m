addpath ../plotters;
addpath ../../mammal;

% prepare data.
% plot synthetic location results
load('tarAttrs.mat')
X = csvread('mammals.txt');
xCoords = X(:, 2);
yCoords = X(:, 1);
X(:, 1:2) = [];
n = size(X, 1);
tarAttrs([13,31,33]) = [];
nrAttrs = 15; 

patternId = 1;

% prepare figure
f = figure('Units','centimeters',...
        'OuterPosition',[3 3 60 13],'Resize','on', 'PaperOrientation',...
        'landscape','PaperType','A4','papersize', [24, 5.5], ...
        'Renderer','painters','Visible', 'off');
lm = 0.01;
rm = 0.01;
chm = 0.02;
width = (1-lm-rm-3*chm)/4;
bm = 0.05;
tm = 0.05;
height = 1-bm-tm;

% 1. plot data
axes('Position',[lm+0.13 bm+0.01 width-0.02 height-0.02],'FontSize',20.0);
hold on;
locationPatternsAfter;
eallAfter = eall;
clearvars p pall eall
locationPatternsBefore;
p = pall(patternId);
% % title = sprintf('id: %d, iter: %d, desc: %s, v: %.5f', p.id,p.iter,p.desc, p.v);
% mammalMapPatterns(xCoords, yCoords, p.idxs, 'figure', f);
% t = title('(a)');
% set(t,'interpreter','none');
% set(gca, 'XTickLabel','', 'YTickLabel','', 'XTick',[],'YTick',[],'XColor','w','YColor','w', 'layer','top');

mammalBoxPlot(X, p, eall{p.iter}, eallAfter{p.iter},tarAttrs, 5, 'figure', f);
plot([-1, -1], [0, 7.5],'w-', 'LineWidth', 1.5);
xlim([-1, 2.5]);
ylim([0, 7.5]);
text(-0.9, 0, '(a)', 'FontSize', 18);



% print(f, '-dpdf', '-fillpage', '2.pdf');
% clearvars p pall eall title d patterns fileName;
rm = rm + width + 0.09 + chm;
width = width - 0.03;
[~, I] = sort(p.SIs, 'descend');
titles = {'(b)', '(c)', '(d)'};
for i = 1:3        
    axes('Position',[rm+(i-1)*(width+chm) bm width height],'FontSize',20.0);
    hold on;
    f = plotMammalAttribute(p.targetCol(I(i)), 'figure', f);
    xrange = xlim;
    yrange = ylim;
    text(xrange(1), yrange(2), titles{i}, 'FontSize', 18);
    
%     set(gca, 'XTickLabel','', 'YTickLabel','', 'XTick',[],'YTick',[],'XColor','w','YColor','w','layer','top');
    set(gca, 'XTickLabel','', 'YTickLabel','', 'XTick',[],'YTick',[],'XColor','w','YColor','w','layer','top');

end
set(gcf, 'Color', 'w');
hold off;


print(f, '-dpdf', '-fillpage', '2.pdf');
clearvars p pall eall title d patterns fileName;


rmpath ../../mammal;
rmpath ../plotters;