function f = barWeights(weights, barLabels, varargin)
    p = inputParser;
    for i = 1:2:length(varargin)
        addParameter(p, lower(varargin{i}), varargin{i+1});
    end    
    parse(p, varargin{:});
    
    if isfield(p.Results, 'figure')
        f = p.Results.figure;
    else
        f = setupFigure();
    end

    width = 0.8;
    faceColor = [70,130,180]/255.;
    bar(weights, width, 'FaceColor', faceColor, 'EdgeColor', 'none');

    set(gca,'XTick',1:length(barLabels));  
    set(gca,'XTickLabel',barLabels);
    set(gca,'TickLabelInterpreter','none'); 
%     a = get (gca,'XTickLabel');
%     set(gca,'XTickLabel',a, 'fontsize',24)
end

