function f = plotMammalAttribute(attrIdx, varargin)
    p = inputParser;
    for i = 1:2:length(varargin)
        addParameter(p, lower(varargin{i}), varargin{i+1});
    end    
    parse(p, varargin{:});   
    
    if isfield(p.Results, 'figure')
        f = p.Results.figure;
    else
        f = setupFigure();
    end 
    
    tarAttrs = load('tarAttrs.mat');
    tarAttrs = tarAttrs.tarAttrs;
    X = csvread('mammals.txt');
    pos = X(:, 1:2);
    X(:, 1:2) = [];
    idxs = find(X(:,attrIdx));
    
    plot(pos(:,2), pos(:,1), 'x', 'MarkerEdgeColor', [171,221,164]/255);
    plot(pos(idxs, 2), pos(idxs,1), 'x', 'MarkerEdgeColor', [215,25,28]/255);    
    axis tight;
%     t = title(sprintf('distribution of %s (%d)', tarAttrs{attrIdx-67,1}, attrIdx));    
%     set(t, 'Interpreter', 'none');
end