% plot synthetic location results
folder = 'figures/mammal/location/';
load('tarAttrs.mat')
X = csvread('mammals.txt');
xCoords = X(:, 2);
yCoords = X(:, 1);
X(:, 1:2) = [];
n = size(X, 1);
tarAttrs([13,31,33]) = [];
nrAttrs = 15;    

% 1. map plot selected patterns
locationPatternsBefore;
for i = 1:length(pall)
    p = pall(i);
    title = sprintf('id: %d, iter: %d, desc: %s, v: %.5f', p.id,p.iter,p.desc, p.v);
    f = mammalMapPatterns(xCoords, yCoords, p.idxs, title);
    print(f, '-dpdf', [folder, 'p', num2str(i), 'a.pdf']);
end
clearvars p pall eall title;

% 2. box plot of background distribtuion before commite a pattern
locationPatternsBefore;
for i = 1:length(pall)
    p = pall(i);
    title = sprintf('Before update, id: %d, iter: %d, desc: %s', p.id,p.iter,p.desc);
    f = mammalBoxPlot(X, p, eall{p.iter}, tarAttrs, 15, title);
    print(f, '-dpdf', [folder, 'p', num2str(i), 'b.pdf']);
end
clearvars p pall eall;

% 3. box plot of background distribtuion after commite a pattern
locationPatternsAfter;
for i = 1:length(pall)
    p = pall(i);
    title = sprintf('Before update, id: %d, iter: %d, desc: %s', p.id,p.iter,p.desc);
    f = mammalBoxPlot(X, p, eall{p.iter}, tarAttrs, 15, title);
    print(f, '-dpdf', [folder, 'p', num2str(i), 'c.pdf']);
end
clearvars p pall eall;

% 4. plot geo information for top 3 attributes of each pattern

locationPatternsAfter;
for i = 1:length(pall)
    p = pall(i);
    [~, I] = sort(p.SIs, 'descend');
    for j = 1:3        
        f = plotMammalAttribute(p.targetCol(I(j)));
        print(f, '-dpdf', [folder, 'p', num2str(i), 'a', num2str(j), '.pdf']);
    end
end
clearvars p pall eall;

clear;