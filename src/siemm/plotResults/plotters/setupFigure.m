function f = setupFigure()
    f = figure('Units','centimeters',...
        'OuterPosition',[3 3 26 20],'Resize','on', 'PaperOrientation',...
        'landscape','PaperType','A4','Renderer','painters','Visible', 'off');
    lm = 0.063;
    rm = 0.027;
    width = 1-lm-rm;
    bm = 0.090;
    tm = 0.02;
    height = 1-bm-tm;    
    axes('Position',[lm bm width height],'FontSize',20);
    set(gca, 'TickDir', 'out');
    hold on;
end

