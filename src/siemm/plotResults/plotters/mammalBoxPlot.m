function f = mammalBoxPlot(X, pattern, equiClasses1, equiClasses2, tarAttrs, nrAttrs, varargin)
    p = inputParser;
    for i = 1:2:length(varargin)
        addParameter(p, lower(varargin{i}), varargin{i+1});
    end    
    parse(p, varargin{:});   
    
    if isfield(p.Results, 'figure')
        f = p.Results.figure;
    else
        f = setupFigure();
    end  
    % prepare to plot top *nrAttrs* attributes according to their SI values
    [~,I] = sort(pattern.SIs, 'descend');
    I = I(1:nrAttrs);
    topAttrIdxs = pattern.targetCol(I);
    topAttrNames = tarAttrs(I);
    for i = 1:nrAttrs
        topAttrNames{i} = topAttrNames{i};
%         topAttrNames{i} = ['(', int2str(topAttrIdxs(i)), ')'];
    end
    positions = 0.5:1.5:1.5*nrAttrs;
    pace = 0.4;  
    
    % plot for subgroup.
    sortedAttrIdxs = topAttrIdxs;
    interval = zeros(length(sortedAttrIdxs), 2);
    interval(:,:) = nan;
    
    for i = 1:length(sortedAttrIdxs)
        values = sort(X(pattern.idxs, sortedAttrIdxs(i)));
        mv = mean(values);
        steps = values(2:end) - values(1:end-1);
        heights = 1:(length(values)-1);
        Z = heights * steps;        
        p = 0.0;
        for j = 1:length(steps)
            p = p + steps(j)*heights(j)/Z;
            if isnan(interval(i,1)) && (p > 0.025)
                interval(i,1) =mv - values(j);
            end
            if isnan(interval(i,2)) && (p >0.975)
                interval(i,2) = values(j+1) - mv;
                break;
            end
        end
%         if (p < 0.975) 
%             interval(i,2) = values(end)- mP(1,i);
%         end        
    end
    mP = mean(X(pattern.idxs, sortedAttrIdxs));
    h1 = errorbar(mP, positions - pace, interval(:,1), interval(:,2), '*', 'horizontal', 'color', [215,25,28]/255);
    set(h1, 'MarkerSize', 10)     

%     boxplot(X(inds.idxs, topAttrIdxs), 'colors','r', 'width', 0.2, 'Labels', topAttrNames, 'positions', positions+pace, 'orientation', 'horizontal');
%     h2 = findall(gca,'Tag','Box');
%     legend([h1(1),h2(end)], {'Model','Pattern'});
%     plot(mean(X(inds.idxs, topAttrIdxs)), positions+pace, 'r*', 'markers', 10);
    set(gca,'Ydir','reverse');    
    h1Xlim = xlim;
    
    % plot for background distribution
    interval = zeros(length(I), 2);
    nrClasses = length(equiClasses1);
    pl = 0.025;
    pu = 0.975;
    weightedMus = zeros(length(I), 1);
    for i = 1:length(I)
        idx = I(i);
        mus = zeros(nrClasses, 1);
        sigmas = zeros(nrClasses, 1);
        weights = zeros(nrClasses, 1);
        for j = 1:nrClasses
            mus(j) = equiClasses1(j).mu(idx);
            sigmas(j) = sqrt(equiClasses1(j).sigma(idx, idx));
            weights(j) = length(intersect(equiClasses1(j).inds, pattern.idxs))/length(pattern.idxs);
        end
        xl = min(norminv(pl-0.01, mus, sigmas));       
        xu = max(norminv(pu+0.01, mus, sigmas));
        range = xl:0.01:xu;
        prob = zeros(length(range), 1);
        for k = 1:length(range)
            prob(k) = normcdf(range(k), mus,sigmas)' * weights;
        end
        xl = find(prob > pl);
        xu = find(prob > pu);
        weightedMu = mus'*weights;
        weightedMus(i, 1) = weightedMu;
        interval(i,:) = [weightedMu - range(xl(1)), range(xu(1)) - weightedMu];
    end
    
    h2 = errorbar(weightedMus, positions, interval(:,1), interval(:,2), 'd', 'horizontal', 'color', [43,131,186]/255);      
    set(h2, 'MarkerSize', 10)
    h2Xlim = xlim;
    hold on;
    
    % plot for background distribution
    interval = zeros(length(I), 2);
    nrClasses = length(equiClasses2);
    pl = 0.025;
    pu = 0.975;
    weightedMus = zeros(length(I), 1);
    for i = 1:length(I)
        idx = I(i);
        mus = zeros(nrClasses, 1);
        sigmas = zeros(nrClasses, 1);
        weights = zeros(nrClasses, 1);
        for j = 1:nrClasses
            mus(j) = equiClasses2(j).mu(idx);
            sigmas(j) = sqrt(equiClasses2(j).sigma(idx, idx));
            weights(j) = length(intersect(equiClasses2(j).inds, pattern.idxs))/length(pattern.idxs);
        end
        xl = min(norminv(pl-0.01, mus, sigmas));       
        xu = max(norminv(pu+0.01, mus, sigmas));
        range = xl:0.01:xu;
        prob = zeros(length(range), 1);
        for k = 1:length(range)
            prob(k) = normcdf(range(k), mus,sigmas)' * weights;
        end
        xl = find(prob > pl);
        xu = find(prob > pu);
        weightedMu = mus'*weights;
        weightedMus(i, 1) = weightedMu;
        interval(i,:) = [weightedMu - range(xl(1)), range(xu(1)) - weightedMu];
    end
    
    h3 = errorbar(weightedMus, positions + pace, interval(:,1), interval(:,2), 'p', 'horizontal', 'color', [253,174,97]/255);      
    set(h3, 'MarkerSize', 10)
    h3Xlim = xlim;
    hold on;
    legend([h1,h2, h3], {'Pattern', 'Model', 'Updated Model'})
    leg = legend('Location', 'best');
    set(leg, 'FontSize', 12)
    yticks(positions);
    yticklabels(strrep(topAttrNames, '_', '\_'));
    
    
    lim = [min([h1Xlim(1), h2Xlim(1), h3Xlim(1)]), max([h1Xlim(2), h2Xlim(2), h3Xlim(2)])];
    xlim(lim);
           
%     t = title(name);
%     set(t, 'Interpreter', 'none');
end

