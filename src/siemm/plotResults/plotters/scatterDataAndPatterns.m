function f = scatterDataAndPatterns(X, patterns, labels, varargin)    
    p = inputParser;
    for i = 1:2:length(varargin)
        addParameter(p, lower(varargin{i}), varargin{i+1});
    end    
    parse(p, varargin{:});
    
    if isfield(p.Results, 'colors')
        colors = p.Results.colors;        
    else
        safeColor;
        colors = color.collection;
    end
    
    if isfield(p.Results, 'figure')
        f = p.Results.figure;
    else
        f = setupFigure();
    end        
    legendLabels = {};
    mX = mean(X,1);        
    safeColor;
    scatter(X(:,1), X(:,2), 40, 'o', 'filled', 'MarkerFaceColor', color.green.rgb, 'MarkerFaceAlpha', 0.4);
%     scatter(X(:,1), X(:,2), 40, 'x', 'MarkerEdgeColor', color.blue.rgb);
    legendLabels{end+1, 1} = 'Data';
    scatter(mX(1), mX(2), 180, 'kp', 'filled');  
    legendLabels{end+1, 1} = 'Data Center';
    markers = {'x', 's'};            
    for i = 1:length(patterns)
%         scatter(X(patterns{i},1), X(patterns{i},2), 32, [colors{i}, markers{1}], 'filled', 'MarkerFaceAlpha', 0.4);
%         scatter(X(patterns{i},1), X(patterns{i},2), 32, [colors{i}, markers{1}], 'filled', 'MarkerFaceAlpha', 0.4);
        scatter(X(patterns{i},1), X(patterns{i},2), 100, markers{1}, 'MarkerEdgeColor', colors{i}, 'LineWidth',2);
        if length(patterns) == 1
            legendLabels{end+1, 1} = 'Pattern';
        else        
            legendLabels{end+1, 1} = ['Pattern ', num2str(i)];
        end
%         mP = mean(X(patterns{i},1:2));
%         scatter(mP(1), mP(2), 150, [colors{i}, markers{2}], 'filled');
    end
    
%     legend(legendLabels);
    if isfield(p.Results, 'w')
        w = p.Results.w;                
        startPoint = [0, 2*w(1)] + mX(1);
        endPoint = [0, 2*w(2)] + mX(2);
        plot(startPoint, endPoint, 'k', 'LineWidth',4);        
    end 
    
    xlabel(labels{1});
    ylabel(labels{2});
end

