function f = contourBgDistAndPattern(X, pattern, equiClasses, attrIds, axixLabels, w, varargin)
     p = inputParser;
    for i = 1:2:length(varargin)
        addParameter(p, varargin{i}, varargin{i+1});
    end    
    parse(p, varargin{:});   
    
    if isfield(p.Results, 'figure')
        f = p.Results.figure;
    else
        f = setupFigure();
    end 
    
    if isfield(p.Results, 'markerSize')
        markerSize = p.Results.markerSize;
    else
        markerSize = 20;
    end 
    
    if isfield(p.Results, 'legendPos')
        legendPos = p.Results.legendPos;
    else
        legendPos = 'best';
    end 
    
    % plot contours
    nrClasses = length(equiClasses);
    mus = cell(nrClasses, 1);
    sigmas = cell(nrClasses, 1);
    fraction = zeros(nrClasses, 1);
    xrange = zeros(2, 1);
    yrange = zeros(2, 1);
    for i = 1:nrClasses
        mus{i} = equiClasses(i).mu(attrIds);
        sigmas{i} = equiClasses(i).sigma(attrIds,attrIds);
        fraction(i) = length(intersect(equiClasses(i).inds, pattern.idxs))/length(pattern.idxs);        
        xrange = [floor(min(xrange(1), mus{i}(1) - 3*sqrt(sigmas{i}(1,1)))), ceil(max(xrange(2), mus{i}(1) + 3*sqrt(sigmas{i}(1,1))))];
        yrange = [floor(min(yrange(1), mus{i}(2) - 3*sqrt(sigmas{i}(2,2)))), ceil(max(yrange(2), mus{i}(2) + 3*sqrt(sigmas{i}(2,2))))];        
    end
    
    [xGrid, yGrid]  = meshgrid(xrange(1):0.1:xrange(2), yrange(1):0.1:yrange(2));
    [n,d] = size(xGrid);
    grid = [reshape(xGrid, n*d, 1), reshape(yGrid, n*d, 1)];
    values = zeros(size(grid,1), 1);
    for i = 1: length(mus)
        if fraction(i) ~= 0
            values = values + fraction(i)*mvnpdf(grid, mus{i}, sigmas{i});
        end
    end
    values = reshape(values, n, d);
    [C, h1] = contour(xGrid, yGrid, values);
    legendInfo{1,1} = 'Bg. dist.';
    c = autumn(100);
    colormap(c(50:100,:));
%     colormap('summer');
%     colormap('autumn');
    hold on;
%     h2 = scatter(X(p.idxs, attrIds(1)), X(p.idxs, attrIds(2)), 32, 'bo', 'filled', 'MarkerFaceAlpha', 0.5);
%     legend(h2, 'Pattern');    
    
%     colors = {'r', 'g', 'b', 'm', 'c', 'y', 'b'};
%     for i = 1:length(equiClasses)
%         ec = equiClasses(i);
%         int = intersect(pattern.idxs, ec.inds);
%         if ~isempty(int)
%             scatter(X(int, attrIds(1)), X(int, attrIds(2)), [colors{i}, 'o']);
%             legendInfo{end+1, 1} = ['class', num2str(i)];
%         end 
%     end

    h2= scatter(X(pattern.idxs, attrIds(1)), X(pattern.idxs, attrIds(2)), ...
        markerSize, 'o', 'filled', 'MarkerFaceColor', [215,25,28]/255);
%         markerSize, 'o', 'filled', 'MarkerFaceColor', [43,131,186]/255);
    legendInfo{end+1, 1} = 'Pattern';
   

    
    xlabel(axixLabels{1});
    ylabel(axixLabels{2});
    
    mP = mean(X(pattern.idxs, attrIds), 1);   
    % plot pattern center
%     h3 = scatter(mP(1), mP(2), 150, 'kp', 'filled');
    h3 =plot(mP(1), mP(2), 'kp','MarkerSize',10, 'MarkerFaceColor','k');
    legendInfo{end+1, 1} = 'Pattern center';
    % plot weight direction        
    startPoint = [0, 8*w(1)] + mP(1);
    endPoint = [0, 8*w(2)] + mP(2);
    h4 = plot(startPoint, endPoint, 'k-', 'LineWidth', 3);    
    
    legendInfo{end+1, 1} = 'w';    
    legend([h1,h2,h3,h4], legendInfo, 'Location', 'best');       
    
end

