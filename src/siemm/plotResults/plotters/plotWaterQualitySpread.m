% plot synthetic location results
folder = 'figures/waterQuality/spread/';
fileName ='waterQuality.csv';
d = importdata(fileName,',', 1);
attributes = d.colheaders;
X = d.data;

% 2. box plot of background distribtuion before commite location step
spreadPatternsBeforeUpdate1;
for i = 1:length(pall)
    p = pall(i);
    title = sprintf('Before update, id: %d, iter: %d, desc: %s', p.id,p.iter,p.desc);
    [~, sortedAttrIdxs] = sort(p.SIs, 'descend');
    f = boxPlotBgDistAndPattern(X, p.idxs, eall{p.iter}, sortedAttrIdxs, attributes(sortedAttrIdxs), title);
    print(f, '-dpdf', [folder, 'p', num2str(i), 'a.pdf']);
end
clearvars p pall eall;

% 3. box plot of background distribtuion after commit location 
spreadPatternsAfterUpdate1;
for i = 1:length(pall)
    p = pall(i);
    title = sprintf('After update, id: %d, iter: %d, desc: %s', p.id,p.iter,p.desc);
    [~, sortedAttrIdxs] = sort(p.SIs, 'descend');
    f = boxPlotBgDistAndPattern(X, p.idxs, eall{p.iter}, sortedAttrIdxs, attributes(sortedAttrIdxs), title);
    print(f, '-dpdf', [folder, 'p', num2str(i), 'b.pdf']);
end
clearvars p pall eall;

% 4. scatter plot patterns with found w after 1st bg distribution update.
spreadPatternsAfterUpdate1;
for i = 1:length(pall)
    p = pall(i);
    title = sprintf('step 2 id: %d, iter: %d, desc: %s, v: %.5f', p.id,p.iter,p.desc);
    [~, attrIds] = sort(abs(p.w), 'descend');
    attrIds = attrIds(1:2);
    w = p.w(attrIds)/norm(p.w(attrIds));
    f = scatterDataAndPatterns(X(:,attrIds), {p.idxs}, title, attributes(attrIds), w);
    print(f, '-dpdf', [folder, 'p', num2str(i), 'c.pdf']);
end
clearvars p pall eall title;

% 5. contour plot of background distribtuion after found w
spreadPatternsAfterUpdate1;
for i = 1:length(pall)
    p = pall(i);
    title = sprintf('After commit location, id: %d, iter: %d, desc: %s', p.id,p.iter,p.desc);
    [~, attrIds] = sort(abs(p.w), 'descend');
    attrIds = attrIds(1:2);
    w = p.w(attrIds)/norm(p.w(attrIds));
    f = contourBgDistAndPattern(X, p, eall{p.iter}, attrIds, title, attributes(attrIds), w);    
    print(f, '-dpdf', [folder, 'p', num2str(i), 'd.pdf']);
end
clearvars p pall eall;

% 6. bar plot of weights
spreadPatternsAfterUpdate1;
for i = 1:length(pall)
    p = pall(i);
    title = sprintf('Weights, id: %d, iter: %d, desc: %s', p.id,p.iter,p.desc);
    f = barWeights(p.w, title, attributes(1:16));
    print(f, '-dpdf', [folder, 'p', num2str(i), 'e.pdf']);
end
clearvars p pall eall;

% 7. plot cumulative distribtuion along weight vector
spreadPatternsAfterUpdate1;
for i = 1:length(pall)
    p = pall(i);
    title = sprintf('Cumulative distribtuion along weight vector, id: %d, iter: %d', p.id,p.iter);
    [~, attrIds] = sort(abs(p.w), 'descend');
    attrIds = attrIds(1:2);
    w = p.w(attrIds)/norm(p.w(attrIds));
    f = plotCDF(X(:, attrIds), p.idxs, eall{p.iter}, attrIds, title, w);
    print(f, '-dpdf', [folder, 'p', num2str(i), 'f.pdf']);
end
clearvars p pall eall;

% 8. scatter plot patterns with found w after 2nd bg distribution update.
spreadPatternsAfterUpdate2;
for i = 1:length(pall)
    p = pall(i);
    title = sprintf('step 2 id: %d, iter: %d, desc: %s, v: %.5f', p.id,p.iter,p.desc);
    [~, attrIds] = sort(abs(p.w), 'descend');
    attrIds = attrIds(1:2);
    w = p.w(attrIds)/norm(p.w(attrIds));
    f = scatterDataAndPatterns(X(:,attrIds), {p.idxs}, title, attributes(attrIds), w);
    print(f, '-dpdf', [folder, 'p', num2str(i), 'h.pdf']);
end
clearvars p pall eall title;

% 9. contour plot of background distribtuion after commit spread
spreadPatternsAfterUpdate2;
for i = 1:length(pall)
    p = pall(i);
    title = sprintf('After commit location, id: %j, iter: %d, desc: %s', p.id,p.iter,p.desc);
    [~, attrIds] = sort(abs(p.w), 'descend');
    attrIds = attrIds(1:2);
    w = p.w(attrIds)/norm(p.w(attrIds));
    f = contourBgDistAndPattern(X, p, eall{p.iter}, attrIds, title, attributes(attrIds), w);    
    print(f, '-dpdf', [folder, 'p', num2str(i), 'i.pdf']);
end
clearvars p pall eall;
