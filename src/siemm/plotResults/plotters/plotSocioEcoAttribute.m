function f = plotSocioEcoAttribute(attributeName, operator, splitPoint)
    fileName ='vote_demo_work_2009.csv';

    d = importdata(fileName,',', 1);
    attributes = d.colheaders;
    X = d.data;
    [data, ~] = loadDataAndConfig();

    f = setupFigure();
    %plot boundary
    plot(data.boundaryX, data.boundaryY);
    scatter(data.xCoords, data.yCoords, 32, 'bo', 'filled', 'MarkerFaceAlpha', 0.5);


    cId = strcmp(attributes, attributeName);
    if strcmp(operator, '>=')
        rIdx = find((X(:,cId) >= splitPoint));
    else
        rIdx = find((X(:,cId) <= splitPoint));
    end
    xCoords = data.xCoords(rIdx);
    yCoords = data.yCoords(rIdx);    
    plot(xCoords, yCoords, 'ro');
    % anotate with district names
    districtNames = data.values(2, rIdx);
     for j = 1:length(rIdx)
         text(xCoords(j), yCoords(j), districtNames(j));
     end
    t = title(sprintf('%s %s %.1f', attributeName, operator, splitPoint));    
    set(t, 'Interpreter', 'none')     

end