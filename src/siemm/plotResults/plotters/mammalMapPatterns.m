function f= mammalMapPatterns(xCoords, yCoords, inds, varargin)
    p = inputParser;
    for i = 1:2:length(varargin)
        addParameter(p, lower(varargin{i}), varargin{i+1});
    end    
    parse(p, varargin{:});   
    
    if isfield(p.Results, 'figure')
        f = p.Results.figure;
    else
        f = setupFigure();
    end  
    
    plot(xCoords, yCoords, 'x', 'MarkerEdgeColor', [171,221,164]/255);
    hold on;
    pxCoords = xCoords(inds);
    pyCoords = yCoords(inds);    
    plot(pxCoords, pyCoords, 'x', 'MarkerEdgeColor', [215,25,28]/255);
    axis tight;
end