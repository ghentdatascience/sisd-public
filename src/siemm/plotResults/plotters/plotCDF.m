function f = plotCDF(X, pinds, equiClasses, attrIds, w, varargin)
    p = inputParser;
    for i = 1:2:length(varargin)
        addParameter(p, varargin{i}, varargin{i+1});
    end    
    parse(p, varargin{:});   
    
    if isfield(p.Results, 'figure')
        f = p.Results.figure;
    else
        f = setupFigure();
    end 
    
    if isfield(p.Results, 'legendPos')
        legendPos = p.Results.legendPos;
    else
        legendPos = 'best';
    end 
    
    nrClasses = length(equiClasses);
    mus = cell(nrClasses, 1);
    sigmas = cell(nrClasses, 1);
    fraction = zeros(nrClasses, 1);
    for i = 1:nrClasses
        mus{i} = equiClasses(i).mu(attrIds);
        sigmas{i} = equiClasses(i).sigma(attrIds,attrIds);
        fraction(i) = length(intersect(equiClasses(i).inds, pinds))/length(pinds);        
    end
    
    l = Inf;
    u = -Inf;
    for i = 1: length(mus)
        if fraction(i) ~= 0         
            mu = mus{i}*w';
            sigma = sqrt(w*sigmas{i}*w');
            l = min(l, mu-3*sigma);
            u = max(u, mu+3*sigma);
        end
    end
    x = linspace(l, u);
    value = zeros(size(x));
    for i = 1: length(mus)
        if fraction(i) ~= 0
            value = value + fraction(i)*normcdf(x, mus{i}*w', sqrt(w*sigmas{i}*w'));
        end
    end
    plot(x, value, 'color', [253,174,97]/255);
    hold on;
    h = cdfplot(X(pinds, :)*w');      
    set(h, 'color', [215,25,28]/255);
    set(gca, 'FontSize', 25);
    leg = legend({'Updated model','Projected data'}, 'Location',legendPos);
    set(leg,'FontSize',25);
    grid off;
end