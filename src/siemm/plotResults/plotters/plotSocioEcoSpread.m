% plot synthetic spread results
folder = 'figures/socioEconomics/spread/';
fileName ='vote_demo_work_2009.csv';
d = importdata(fileName,',', 1);
attributes = d.colheaders;
X = d.data;
[data, ~] = loadDataAndConfig();

% 1. map plot selected patterns
spreadPatternsBeforeUpdate1;
for i = 1:length(pall)
    p = pall(i);
    title = sprintf('id: %d, iter: %d, desc: %s, v: %.5f', p.id,p.iter,p.desc, p.v);
    f = germanMapPatterns(data.xCoords, data.yCoords, p.idxs, title, data.values(2, p.idxs), {data.boundaryX, data.boundaryY});
    print(f, '-dpdf', [folder, 'p', num2str(i), 'a.pdf']);
end
clearvars p pall eall title;

% 2. box plot of background distribtuion before commite location step
spreadPatternsBeforeUpdate1;
for i = 1:length(pall)
    p = pall(i);
    title = sprintf('Before update, id: %d, iter: %d, desc: %s', p.id,p.iter,p.desc);
    [~, sortedAttrIdxs] = sort(p.SIs, 'descend');
    f = boxPlotBgDistAndPattern(X, p.idxs, eall{p.iter}, sortedAttrIdxs, attributes(sortedAttrIdxs), title);
    print(f, '-dpdf', [folder, 'p', num2str(i), 'b.pdf']);
end
clearvars p pall eall;

% 3. box plot of background distribtuion after commit location 
spreadPatternsAfterUpdate1;
for i = 1:length(pall)
    p = pall(i);
    title = sprintf('After update, id: %d, iter: %d, desc: %s', p.id,p.iter,p.desc);
    [~, sortedAttrIdxs] = sort(p.SIs, 'descend');
    f = boxPlotBgDistAndPattern(X, p.idxs, eall{p.iter}, sortedAttrIdxs, attributes(sortedAttrIdxs), title);
    print(f, '-dpdf', [folder, 'p', num2str(i), 'c.pdf']);
end
clearvars p pall eall;

% 4. scatter plot patterns with found w after 1st bg distribution update.
spreadPatternsAfterUpdate1;
for i = 1:length(pall)
    p = pall(i);
    title = sprintf('step 2 id: %d, iter: %d, desc: %s, v: %.5f', p.id,p.iter,p.desc);
    attrIds = find(p.w ~= 0);
    f = scatterDataAndPatterns(X(:,attrIds), {p.idxs}, title, attributes(attrIds), p.w(attrIds));
    print(f, '-dpdf', [folder, 'p', num2str(i), 'd.pdf']);
end
clearvars p pall eall title;

% 5. contour plot of background distribtuion after found w
spreadPatternsAfterUpdate1;
for i = 1:length(pall)
    p = pall(i);
    title = sprintf('After commit location, id: %d, iter: %d, desc: %s', p.id,p.iter,p.desc);
    attrIds = find(p.w ~= 0);
    f = contourBgDistAndPattern(X, p, eall{p.iter}, attrIds, title, attributes(attrIds), p.w(attrIds));    
    print(f, '-dpdf', [folder, 'p', num2str(i), 'e.pdf']);
end
clearvars p pall eall;

% 6. bar plot of weights
spreadPatternsAfterUpdate1;
for i = 1:length(pall)
    p = pall(i);
    title = sprintf('Weights, id: %d, iter: %d, desc: %s', p.id,p.iter,p.desc);
    f = barWeights(p.w, title, attributes);
    print(f, '-dpdf', [folder, 'p', num2str(i), 'f.pdf']);
end
clearvars p pall eall;

% 7. plot cumulative distribtuion along weight vector
spreadPatternsAfterUpdate1;
for i = 1:length(pall)
    p = pall(i);
    title = sprintf('Cumulative distribtuion along weight vector, id: %d, iter: %d', p.id,p.iter);
    attrIds = find(p.w ~= 0);
    f = plotCDF(X(:, attrIds), p.idxs, eall{p.iter}, attrIds, title, p.w(attrIds));
    print(f, '-dpdf', [folder, 'p', num2str(i), 'g.pdf']);
end
clearvars p pall eall;

% 7. plot cumulative distribtuion along weight vector
spreadPatternsAfterUpdate1;
for i = 1:length(pall)
    p = pall(i);
    title = sprintf('Variance along each vector, id: %d, iter: %d', p.id,p.iter);
    attrIds = find(p.w ~= 0);
    f = plotVariances(X(:, attrIds), p.idxs, eall{p.iter}, title, p.w(attrIds));
    print(f, '-dpdf', [folder, 'p', num2str(i), 'g1.pdf']);
end
clearvars p pall eall;

% 8. (1st pattern) scatter plot patterns with found w after 2nd bg distribution update.
spreadPatternsAfterUpdate2;
for i = 1:length(pall)
    p = pall(i);
    title = sprintf('step 2 id: %d, iter: %d, desc: %s, v: %.5f', p.id,p.iter,p.desc);
    attrIds = find(p.w ~= 0);
    f = scatterDataAndPatterns(X(:,attrIds), {p.idxs}, title, attributes(attrIds), p.w(attrIds));
    print(f, '-dpdf', [folder, 'p', num2str(i), 'h.pdf']);
end
clearvars p pall eall title;

% 9. (1st pattern) contour plot of background distribtuion after commit spread
spreadPatternsAfterUpdate2;
for i = 1:length(pall)
    p = pall(i);
    title = sprintf('After commit location, id: %j, iter: %d, desc: %s', p.id,p.iter,p.desc);
    attrIds = find(p.w ~= 0);
    f = contourBgDistAndPattern(X, p, eall{p.iter}, attrIds, title, attributes(attrIds), p.w(attrIds));    
    print(f, '-dpdf', [folder, 'p', num2str(i), 'i.pdf']);
end
clearvars p pall eall;

% 10. plot attributes for each pattern 
f = plotSocioEcoAttribute('Children Pop.', '<=', 14.8);
print(f, '-dpdf', [folder, 'p1a1.pdf']);
f = plotSocioEcoAttribute('Mid-aged Pop.', '>=', 26.5);
print(f, '-dpdf', [folder, 'p2a1.pdf']); 
f = plotSocioEcoAttribute('Service workf.', '<=', 62.8);
print(f, '-dpdf', [folder, 'p3a1.pdf']); 

clear;