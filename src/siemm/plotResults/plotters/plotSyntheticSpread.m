% plot synthetic spread results
folder = 'figures/synthetic/spread/';
fileName = 'spreadData.csv';
d = importdata(fileName,',', 1);
attributes = d.colheaders;
X = d.data;
patterns = cell(3,1);
for i = 1:length(patterns)
    patterns{i} = find(X(:, i+2) == 1);
end
% 1. plot data
f = scatterDataAndPatterns(X, patterns, 'synthetic data', {'1', '2'});
print(f, '-dpdf', [folder, 'data.pdf']);
clearvars d patterns fileName;

% 2. scatter plot selected location patterns 
spreadPatternsBeforeUpdate1;
for i = 1:length(pall)
    p = pall(i);
    title = sprintf('step 1 id: %d, iter: %d, desc: %s', p.id,p.iter,p.desc);
    f = scatterDataAndPatterns(X, {p.idxs}, title, {'1', '2'});
    print(f, '-dpdf', [folder, 'p', num2str(i), 'a.pdf']);
end
clearvars p pall eall title;

% 3. (1st pattern) contour plot of background distribtuion before commit location 
spreadPatternsBeforeUpdate1;
for i = 1:length(pall)
    p = pall(i);
    title = sprintf('Before commit location, id: %d, iter: %d, desc: %s', p.id,p.iter,p.desc);
    f = contourBgDistAndPattern(X, p, eall{p.iter}, 1:2, title, {'1', '2'});    
    xlim([-5 5]);
    ylim([-5 5]);
    print(f, '-dpdf', [folder, 'p', num2str(i), 'b.pdf']);
end
clearvars p pall eall;

% 4. contour plot of background distribtuion after commit location
spreadPatternsAfterUpdate1;
for i = 1:length(pall)
    p = pall(i);
    title = sprintf('After commit location, id: %d, iter: %d, desc: %s', p.id,p.iter,p.desc);
    f = contourBgDistAndPattern(X, p, eall{p.iter}, 1:2, title, {'1', '2'});    
    xlim([-5 5]);
    ylim([-5 5]);
    print(f, '-dpdf', [folder, 'p', num2str(i), 'c.pdf']);
end
clearvars p pall eall;

% 5. scatter plot patterns with found w after 1st bg distribution update.
spreadPatternsAfterUpdate1;
for i = 1:length(pall)
    p = pall(i);
    title = sprintf('step 2 id: %d, iter: %d, desc: %s, v: %.5f', p.id,p.iter,p.desc);
    f = scatterDataAndPatterns(X, {p.idxs}, title, {'1', '2'}, p.w);
    print(f, '-dpdf', [folder, 'p', num2str(i), 'd.pdf']);
end
clearvars p pall eall title;

% 6. contour plot of background distribtuion after found w
spreadPatternsAfterUpdate1;
for i = 1:length(pall)
    p = pall(i);
    title = sprintf('After commit location, id: %d, iter: %d, desc: %s', p.id,p.iter,p.desc);
    attrIds = find(p.w ~= 0);
    f = contourBgDistAndPattern(X, p, eall{p.iter}, attrIds, title, attributes(attrIds), p.w(attrIds));    
    print(f, '-dpdf', [folder, 'p', num2str(i), 'e.pdf']);
end
clearvars p pall eall;

% 7. bar plot of weights
spreadPatternsAfterUpdate1;
for i = 1:length(pall)
    p = pall(i);
    title = sprintf('Weights, id: %d, iter: %d, desc: %s', p.id,p.iter,p.desc);
    f = barWeights(p.w, title, {'1', '2'});
    print(f, '-dpdf', [folder, 'p', num2str(i), 'f.pdf']);
end
clearvars p pall eall;

% 8. plot cumulative distribtuion along weight vector
spreadPatternsAfterUpdate1;
for i = 1:length(pall)
    p = pall(i);
    title = sprintf('Cumulative distribtuion along weight vector, id: %d, iter: %d', p.id,p.iter);
    attrIds = find(p.w ~= 0);
    f = plotCDF(X(:, attrIds), p.idxs, eall{p.iter}, attrIds, title, p.w(attrIds));
    print(f, '-dpdf', [folder, 'p', num2str(i), 'g.pdf']);
end
clearvars p pall eall;

% 9. (1st pattern) scatter plot patterns with found w after 2nd bg distribution update.
spreadPatternsAfterUpdate2;
for i = 1:1
    p = pall(i);
    title = sprintf('step 2 id: %d, iter: %d, desc: %s, v: %.5f', p.id,p.iter,p.desc);
    f = scatterDataAndPatterns(X, {p.idxs}, title, {'1', '2'}, p.w);
    print(f, '-dpdf', [folder, 'p', num2str(i), 'h.pdf']);
end
clearvars p pall eall title;

% 10. (1st pattern) contour plot of background distribtuion after commit spread
spreadPatternsAfterUpdate2;
for i = 1:1
    p = pall(i);
    title = sprintf('After commit location, id: %d, iter: %d, desc: %s', p.id,p.iter,p.desc);
    f = contourBgDistAndPattern(X, p, eall{p.iter}, 1:2, title, {'1', '2'}, p.w);    
    xlim([-5 5]);
    ylim([-5 5]);
    print(f, '-dpdf', [folder, 'p', num2str(i), 'i.pdf']);
end
clearvars p pall eall;

clear;