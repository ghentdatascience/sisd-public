function f= germanMapPatterns(xCoords, yCoords, inds, annInds, annotations, boundary, varargin)
    p = inputParser;
    for i = 1:2:length(varargin)
        addParameter(p, varargin{i}, varargin{i+1});
    end    
    parse(p, varargin{:});   
    
    if isfield(p.Results, 'figure')
        f = p.Results.figure;
    else
        f = setupFigure();
    end 
    
    if isfield(p.Results, 'markerSize')
        markerSize = p.Results.markerSize;
    else
        markerSize = 20;
    end 
    
    
    plot(boundary{1}, boundary{2}, '-', 'color', [43,131,186]/255);
    scatter(xCoords, yCoords, markerSize, 'o', 'filled', 'MarkerFaceColor', [171,221,164]/255);
    hold on;
    pxCoords = xCoords(inds);
    pyCoords = yCoords(inds);   
    if isfield(p.Results, 'values')
        coloeScheme = parula(length(p.Results.values));
        [~,I] = sort(p.Results.values - mean(p.Results.values));
        scatter(pxCoords, pyCoords, markerSize, coloeScheme(I,:), 'o', 'filled');
    else
        scatter(pxCoords, pyCoords, markerSize, 'ro', 'filled', 'MarkerFaceColor', [215,25,28]/255);
    end 
    axis tight;
    
    
    for i = 1:length(annotations)
        text(xCoords(annInds(i)), yCoords(annInds(i)), annotations(i), 'FontSize', 10);
    end

    
end