color.red.rgb = [215,25,28]/255;
color.orange.rgb = [253,174,97]/255;
color.green.rgb = [171,221,164]/255;
color.blue.rgb = [43,131,186]/255;
color.collection = {color.red.rgb, color.orange.rgb, color.blue.rgb, color.green.rgb};