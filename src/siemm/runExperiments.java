package siemm;

import java.io.*;

import matlabcontrol.*;
import siemm.plotResults.synthetic.*;

public class runExperiments {
	@SuppressWarnings("static-access")
	public static void main(String[] args) throws FileNotFoundException {
		// run spread pattern mining on synthetic data.
		System.out.println("Synthetic data mining experiment ... ");
		siemm.synthetic.spreadCaseStudy syntheticSpreadCaseStudy = new siemm.synthetic.spreadCaseStudy();
		syntheticSpreadCaseStudy.main(args);
		System.out.println("done.");
		
		// test the effectiveness of subjective interestingness.
		System.out.println("Synthetic data SI experiment ... ");
		siemm.plotResults.synthetic.result2 syntheticResult2 = new result2();
		syntheticResult2.main(args);
		System.out.println("done.");
		
		// [running time 10 min] distortion test and compare to the random baseline.
		System.out.println("Synthetic data robustness experiment ... start");
		siemm.plotResults.synthetic.result3 syntheticResult3 = new result3();
		syntheticResult3.main(args);
		System.out.println("done.");
				
		// test the stableness of finding optimal weight vecto w 
		System.out.println("Synthetic data optimization satbleness experiment ... ");
		siemm.plotResults.synthetic.result4 syntheticResult4 = new result4();
		syntheticResult4.main(args);
		System.out.println("done.");
		
		// [running time 20min] run location pattern mining on mammal data.
		System.out.println("Mammal data mining experiment ... ");
		siemm.mammal.locationCaseStudy mammalLocationCaseStudy = new siemm.mammal.locationCaseStudy();
		mammalLocationCaseStudy.main(args);
		System.out.println("done.");
		
		// run spread pattern mining on German Socio-economics data.
		System.out.println("German Socio-economics data mining experiment ... ");
		siemm.germanSocioEconomics.spreadCaseStudy germanSpreadCaseStudy = new siemm.germanSocioEconomics.spreadCaseStudy();
		germanSpreadCaseStudy.main(args);
		System.out.println("done.");
				
		// run spread pattern mining on water quality data.
		System.out.println("Water quality data mining experiment ... ");
		siemm.waterQuality.spreadCaseStudy waterQualitySpreadCaseStudy = new siemm.waterQuality.spreadCaseStudy();
		waterQualitySpreadCaseStudy.main(args);
		System.out.println("done.");
		
		
		MatlabProxyFactory factory = new MatlabProxyFactory();					
		try {
			System.out.print("Plotting figures  ... ");
			MatlabProxy proxy = factory.getProxy();			
			proxy.eval("cd src/siemm/plotResults;");
			proxy.eval("plotResults;");
			proxy.eval("cd ../../../");
			proxy.exit();
			System.out.println("done.");
		} catch (MatlabInvocationException | MatlabConnectionException e) {
			e.printStackTrace();
		}		
		
	}
}
