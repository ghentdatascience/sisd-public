function [bestW, bestV, attributeSI] = siSpreadChiSquared(inds, target, sallObject)  
    attributeSI = [];
	% Because the starting index differ 1 between java and matlab.
    inds = inds + 1;
    % convert java input into arry of objects. 
    target = cell2mat(target');
    sall = constructEquiClasses(sallObject);
%     save('workspace.mat', 'inds', 'target', 'sall');
    % compute extension of the target data.
    ext = target(inds,:);
    [n, k] = size(ext);      
    
    % compute background distribution parameters
    diffs = {};
    sigmas = {};
    for s = sall
        int = intersect(s.inds, inds);
        if ~isempty(int)
            sigmas{end+1, 1} = s.S;
            diffs{end+1, 1} = target(int,:) - repmat(s.mu', length(int), 1);
        end
    end    

    nonZeroPos = nchoosek(1:k, 2);
    nonZeroPos = [2,5];
    bestV = -Inf;
    bestW = zeros(2,1);
    vs = zeros(10, 360);

    for i = 1:size(nonZeroPos,1)
        for l = 1:360
            w = zeros(k, 1);
            w(nonZeroPos(i,:)) = [cos(l/180*pi), sin(l/180*pi)]';
            v = sicSpread(w, n, diffs, sigmas);        
            if v > bestV
                bestW = w;
                bestV = v;
            end
            vs(i,l) = v;            
        end
    end
%     bestV
%     bestW
%     save('vs','vs');

%     nonZeroPos = nchoosek(1:k, 2); 
%     bestW = zeros(k, 1);
%     bestV = -Inf;
%     for i = 1:size(nonZeroPos, 1)
%         % Create the problem structure.
% %         manifold = stiefelfactory(2,1);
%         manifold = spherefactory(2,1); 
%         problem.M = manifold;
%         
%         % Define the problem cost function
%         problem.cost = @(w) -obj(w, n, diff, card, delta, avgS, nonZeroPos(i,:));
% %         problem.egrad = @(W) -gradient(W, A, B, n);
% 
%         % Numerically check gradient consistency (optional).
% %         figure;
% %         checkgradient(problem);
% 
%         % Solve.
%         options.verbosity = 0;
%         options.maxiter = 500;
%         [w, v, ~, ~] = pso(problem, [], options);
%         
%        if -v > bestV
%            bestV = -v;
%            bestW = zeros(k, 1);
%            bestW(nonZeroPos(i,:), 1) = w;
%        end
%     end
    
    attributeSI = zeros(k ,1);
    for i = 1:k 
        e = zeros(k,1); 
        e(i) = 1;
        attributeSI(i, 1) = sicSpread(e, n, diffs, sigmas);
    end 
    
    function vectors = vectorOnUnitSphere(n, dim)
        vectors = randn(n,dim);
        vectors = normr(vectors);
    end

    function value = obj(w,n, diff, card, delta, avgS, nonZeroPos)
        fullW = zeros(size(avgS,1),1);
        fullW(nonZeroPos,1) = w';
        w = fullW;
        value = sicSpread(w,n, diff, card, delta, avgS);
    end

    function value = sicSpread(w,n, diffs, sigmas)
        c = 0.;
        for j = 1:length(diffs)
            c = c + sum((diffs{j}*w).^2)/(w'*sigmas{j}*w);
        end
        
        value = n/2*log(2) + gammaln(n/2) - n - (n/2-1)*log(c*n) + c*n/(2);
%         cbar = w'*avgS*w;
%         a = 0.;
%         for j = 1:length(delta)
%            a = a + card(j,1)*(w'*delta{j,1}*w)^2; 
%         end
%         alpha = cbar + 2*a/(n*cbar);
%         beta = a/cbar;
%         d = n;
%         T = norm(diff*w)^2/n;
%         value = -(d/2-1)*log((T-beta)/alpha) + (T-beta)/alpha;
    end
end