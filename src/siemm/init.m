function sall = init(X)
    X = cell2mat(X');
    [n,~] = size(X);
    % Throughout the mining process, the algorithm keeps a struct array,
    % with an element for each equivalence class,
    % and containing the following fields:

    % The covariance matrix, here initialized to the overall covariance matrix:
    s.S = cov(X);
    
    % Minus a half the inverse covariance matrix (natural parameter)
    s.Sminhalfinv = -1/2*inv(s.S);
    % The mean, here initialized to the overall mean:
    s.mu = mean(X,1)';
    % The inverse covariance times the mean (natural parameter)
    s.Sinvmu = -2*s.Sminhalfinv*s.mu;
    % The set of points part of this equivalence class
    s.inds = 1:n;

    % Start off with just a single equivalence class.
    sall(1) = s;    
end