randomPointSize = 500;
patternSize = 40;
dataSize = randomPointSize + 2*patternSize;
p = 1;


X = mvnrnd([0, 0], [1 0; 0 1], randomPointSize);
p1 = mvnrnd([2,2], [0.05, 0; 0, 0.05], patternSize);
p2 = mvnrnd([-1,0], [0.05, 0; 0, 0.05], patternSize);


figure;
plot(X(:,1), X(:,2), 'bx');
hold on;
plot(p1(:,1), p1(:,2), 'ro');
plot(p2(:,1), p2(:,2), 'go');

f1 = zeros(dataSize,1);
f1(end-2*patternSize+1:end-patternSize) = double(binornd(1, p, patternSize, 1));
f1(end-patternSize+1:end) = 0;

f2 = zeros(dataSize,1);
f2(end-2*patternSize+1:end-patternSize) = 0;
f2(end-patternSize+1:end) = double(binornd(1, p, patternSize, 1));

f3 = randi(2,dataSize,1) - 1;
f4 = randi(2,dataSize,1) - 1;

result = [X;p1;p2];
result = [result, f1, f2, f3, f4];
result = vertcat(1:6, result);
csvwrite('locationData.csv', result);




