randomPointSize = 500;
patternSize = 40;
dataSize = randomPointSize + 3*patternSize;
p = 1.0;


X = mvnrnd([0, 0], [1 0; 0 1], randomPointSize);
% p1 = mvnrnd([2,2], [0.05, 0; 0, 0.05], patternSize);
% p2 = mvnrnd([-1,0], [0.05, 0; 0, 0.05], patternSize);

theta = pi/2;
R = [cos(theta), -sin(theta); sin(theta), cos(theta)];
S = R\[0.75, 0; 0, 0.075]*R;
mu = [-2,0];
p1 = mvnrnd(mu, S, patternSize);

theta = pi/2;
R = [cos(theta), -sin(theta); sin(theta), cos(theta)];
S = R\[0.25, 0; 0, 0.025]*R;
mu = [2^.5, 2^.5];
p2 = mvnrnd(mu, S, patternSize);

theta = pi*2/3;
R = [cos(theta), -sin(theta); sin(theta), cos(theta)];
S = R\[0.5, 0; 0, 0.05]*R;
mu = [0, -2];
% mu = [0, 0];
p3 = mvnrnd(mu, S, patternSize);

figure;
plot(X(:,1), X(:,2), 'bx');
hold on;
plot(p1(:,1), p1(:,2), 'ro');
plot(p2(:,1), p2(:,2), 'go');
plot(p3(:,1), p3(:,2), 'mo');

f1 = zeros(dataSize,1);
f1(end-3*patternSize+1:end-2*patternSize) = double(binornd(1, p, patternSize, 1));
f1(end-2*patternSize+1:end-patternSize) = 0;
f1(end-patternSize+1:end) = 0;

f2 = zeros(dataSize,1);
f2(end-3*patternSize+1:end-2*patternSize) = 0;
f2(end-2*patternSize+1:end-patternSize) = double(binornd(1, p, patternSize, 1));
f2(end-patternSize+1:end) = 0;

f3 = zeros(dataSize,1);
f3(end-3*patternSize+1:end-2*patternSize) = 0;
f3(end-2*patternSize+1:end-patternSize) = 0;
f3(end-patternSize+1:end) = double(binornd(1, p, patternSize, 1));


f4 = randi(2,dataSize,1) - 1;
f5 = randi(2,dataSize,1) - 1;

result = [X;p1;p2;p3];
result = [result, f1, f2, f3, f4, f5];
result = vertcat(1:7, result);
% csvwrite('spreadData.csv', result);




