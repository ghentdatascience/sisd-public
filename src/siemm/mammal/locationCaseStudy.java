package siemm.mammal;

import java.io.*;
import java.util.*;

import nl.liacs.subdisc.*;
import siemm.*;

public class locationCaseStudy {
	public static void main(String[] args) throws FileNotFoundException {			
		Table table = new FileLoaderARFF(new File("src/siemm/mammal/mammals_disable_pos.arff")).getTable();
		table.update();
		List<Integer> idxs = new ArrayList<>();
		for (int i = 67; i < 191; i++) {
			idxs.add(i);
		}
		idxs.remove(32);
		idxs.remove(30);
		idxs.remove(12);
		int[] targetIdxs = new int[121];
		for (int i = 0; i < idxs.size(); i++) {
			targetIdxs[i] = idxs.get(i);
		}

		SearchParameters searchParameters = configSearchParameters(table, targetIdxs);		
		Miner miner = new Miner(table, targetIdxs, searchParameters, "src/siemm/mammal/locationPatterns.m");
		miner.mineLocationPatterns(3);
	}		

	private static SearchParameters configSearchParameters(Table table, int[] targetIdxs) {
		SearchParameters searchParameters = new SearchParameters();
		searchParameters.setTargetConcept(configTargetConcept(table, targetIdxs));			
		
		searchParameters.setSearchDepth(4);
		searchParameters.setMinimumCoverage(10);
		searchParameters.setQualityMeasureMinimum((float) 1.0);
		searchParameters.setMaximumCoverageFraction((float) 0.8);
		searchParameters.setMaximumSubgroups(150);
		searchParameters.setMaximumTime((float) 5);		

		searchParameters.setSearchStrategy("beam");
		searchParameters.setSearchStrategyWidth(40);
		searchParameters.setNominalSets(false);
		searchParameters.setNumericStrategy("bins");
		searchParameters.setNumericOperators("<html>&#8804;, &#8805;</html>");
		searchParameters.setNrBins(5);
		
		return searchParameters;
	}

	private static TargetConcept configTargetConcept(Table table, int[] targetIdxs) {

		TargetConcept targetConcept = new TargetConcept();
		targetConcept.setTargetType("multi-label");
		List<Column> multiTargets = new ArrayList<>();
		for (int idx : targetIdxs) {
			multiTargets.add(table.getColumn(idx));
		}
		
		targetConcept.setMultiTargets(multiTargets);
		return targetConcept;
	}

}
