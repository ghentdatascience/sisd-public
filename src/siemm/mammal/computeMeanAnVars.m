function [mu, var] = computeMeanAnVars(equiClasses, n)
    mu = zeros(size(equiClasses(1).mu));
    var = zeros(size(equiClasses(1).var));
    for i = 1:length(equiClasses)
        class = equiClasses(i);
        s = length(class.inds);
        mu = mu + s/n * class.mu;
        var = var + (s/n)^2 * class.var;
    end
end

