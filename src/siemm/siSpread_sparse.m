function [bestW, bestV, attributeSI] = siSpread_sparse(inds, target, sallObject)     
	% Because the starting index differ 1 between java and matlab.
    inds = inds + 1;
    % convert java input into arry of objects. 
    target = cell2mat(target');
    sall = constructEquiClasses(sallObject);
%     save('workspace.mat', 'inds', 'target', 'sall');
    % compute extension of the target data
    
    ext = target(inds,:);
    [n, k] = size(ext);      
    
    % compute background distribution parameters
    delta = {};
    diff = [];
    avgS = zeros(k,k);
    card = [];
    for s = sall
        int = intersect(s.inds, inds);
        if ~isempty(int)
            card(end+1,1) = length(int);                        
%             delta{end+1,1} = card(end,1)*s.S/n;            
            delta{end+1,1} = s.S/n;            
            avgS = avgS + card(end,1)*delta{end, 1};
            diff = [diff; target(int,:) - repmat(s.mu', card(end, 1), 1)];

        end
    end
    avgS = avgS/n;
    for i = 1:length(delta)
        delta{i,1} = delta{i,1} - avgS;
    end
       
    manifold = spherefactory(2,1); 
    problem.M = manifold;
    
    % Solve.
    options.verbosity = 0;
    options.maxiter = 1000;
    
    nonZeroPos = nchoosek(1:k, 2);
    bestV = -Inf;    
    for i = 1:size(nonZeroPos,1)
        % Define the problem cost function
        diff2d = diff(:, nonZeroPos(i,:));
        avgS2d = avgS(nonZeroPos(i,:), nonZeroPos(i,:));
        delta2d = cell(size(delta));
        for l = 1:length(delta)
            delta2d{l} = delta{l}(nonZeroPos(i,:), nonZeroPos(i,:));
        end
        problem.cost = @(w) -sicSpread(w, n, diff2d, card, delta2d, avgS2d);
        problem.egrad = @(w) -gradient(w, n, diff2d, card, delta2d, avgS2d);
%         % Numerically check gradient consistency (optional).
%         figure;
%         checkgradient(problem);
        [tempW, ~, ~, ~] = steepestdescent(problem, [], options);

        v = sicSpread(tempW, n, diff2d, card, delta2d, avgS2d);
        if v > bestV 
            bestW = zeros(k, 1);
            bestW(nonZeroPos(i,:),1) = tempW;            
            bestV = v;
        end
    end
    
    attributeSI = zeros(k ,1);
    for i = 1:k 
        e = zeros(k,1); 
        e(i) = 1;
        attributeSI(i, 1) = sicSpread(e, n, diff, card, delta, avgS);
    end 
        
    function value = sicSpread(w, n, diff, card, delta, avgS)       
        abar = w'*avgS*w;
        delta2Sum = 0.;
        for j = 1:length(delta)
           delta2Sum = delta2Sum + card(j,1)*(w'*delta{j,1}*w)^2; 
        end
        alpha = abar + 2*delta2Sum/(n*abar);
        beta = delta2Sum/abar;
        d = n;
        tws = norm(diff*w)^2/n;
        value = d/2*log(2) + gammaln(d/2) + alpha - (d/2-1)*log((tws-beta)/alpha) + (tws-beta)/(2*alpha);
    end

    function g = gradient(w, n, diff, card, delta, avgS)  
        pDelta = {};
        pBeta = zeros(size(w));
        
        abar = w'*avgS*w;
        pAbar = 2*avgS*w;        
        delta2Sum = 0.;
        for j = 1:length(delta)
            pDelta{end+1, 1} = 2*delta{j}*w;  
            deltaj = w'*delta{j}*w;
            delta2Sum = delta2Sum + card(j)*deltaj^2;             
            pBeta = pBeta + (2*card(j)*deltaj*abar*pDelta{end,1} - card(j)*deltaj^2*pAbar)/(abar^2);            
        end
        pAlpha = pAbar + 2/n*pBeta;
        alpha = abar + 2*delta2Sum/(n*abar);
        beta = delta2Sum/abar;
        pTws = 2/n * (diff'*diff)*w;
        tws = norm(diff*w)^2/n;
        pc = ((pTws-pBeta)*alpha - pAlpha*(tws - beta))/(alpha^2);
        g = pAlpha - (n/2 - 1) * alpha/(tws - beta) * pc + 1./2*pc;
    end
end