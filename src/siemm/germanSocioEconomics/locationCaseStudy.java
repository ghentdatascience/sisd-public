package siemm.germanSocioEconomics;

import java.io.*;
import java.util.*;

import nl.liacs.subdisc.*;
import siemm.*;

public class locationCaseStudy {
	public static void main(String[] args) throws FileNotFoundException {
		Table table = new DataLoaderTXT(new File("src/siemm/germanSocioEconomics/vote_demo_work_2009.csv")).getTable();
		table.update();
		int[] targetIdxs = {0,1,2,3,4};
		SearchParameters searchParameters = configSearchParameters(table, targetIdxs);		
		Miner miner = new Miner(table, targetIdxs, searchParameters, "src/siemm/germanSocioEconomics/locationPatterns.m");
		miner.mineLocationPatterns(3);	
	}	
	private static SearchParameters configSearchParameters(Table table, int[] targetIdxs) {
		SearchParameters searchParameters = new SearchParameters();
		searchParameters.setTargetConcept(configTargetConcept(table, targetIdxs));		
		
		searchParameters.setSearchDepth(4);
		searchParameters.setMinimumCoverage(5);
		searchParameters.setQualityMeasureMinimum((float) 1.0);
		searchParameters.setMaximumCoverageFraction((float) 0.8);
		searchParameters.setMaximumSubgroups(100);
		searchParameters.setMaximumTime((float) 5);		

		searchParameters.setSearchStrategy("beam");
		searchParameters.setSearchStrategyWidth(40);
		searchParameters.setNominalSets(false);
		searchParameters.setNumericStrategy("bins");
		searchParameters.setNumericOperators("<html>&#8804;, &#8805;</html>");
		searchParameters.setNrBins(5);
		
		return searchParameters;
	}	
	private static TargetConcept configTargetConcept(Table table, int[] targetIdxs) {
		TargetConcept targetConcept = new TargetConcept();
		targetConcept.setTargetType("multi-label");
		List<Column> multiTargets = new ArrayList<>();
		for (int idx : targetIdxs) {
			multiTargets.add(table.getColumn(idx));
		}		
		targetConcept.setMultiTargets(multiTargets);
		return targetConcept;
	}

}
