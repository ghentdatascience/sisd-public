function [data, config] = loadDataAndConfig()
%% read data

% Read raw data file.
fid = fopen('socio_economics_germany_2009_v3.csv');
rawData = textscan(fid, '%s', 'Delimiter', ';');
rawData = reshape(rawData{1}, 45, 413);

%split rawData ionto attribts and values
data.attributes = rawData(:,1);
data.values = rawData(:,2:end);
data.size = size(data.values, 2);

% feature group indices
data.featureGroupNames = {'vote', 'workForce', 'demographic'};
data.featureGroupToDataColMap = containers.Map(data.featureGroupNames, {14:18, 35:42, 21:25});

% prepare data matrix with selected feature group.
data.getDataMatrixWithSelectedFeatureGroup = @compileDataMatrix;
    function X = compileDataMatrix(featureGroupName)
        X = data.values(data.featureGroupToDataColMap(featureGroupName), :);
        X = strrep(X, ',', '.');
        X = str2double(X);
        X = X';
        X = zscore(X);
    end

% prepare geographic coordinates of districs
coordinates = computeCoordinates(data.values, data.size);
data.xCoords = -coordinates(:,1);
data.yCoords = coordinates(:,2);
    function coordinates = computeCoordinates(dataValues, dataSize)
        % compile coordinates of each district
        coordinates = zeros(dataSize, 2);
        rawCoords = char(dataValues(7,:));
        for k = 1:dataSize
            coordinate = strsplit(rawCoords(k,:), ',');
            coordinates(k,:) = str2double(coordinate);
        end
        % rotate the coordinates
        angle = pi/2;
        T = [cos(angle) sin(angle); cos(angle+pi/2) sin(angle+pi/2)];
        coordinates = zscore(coordinates) * T;
    end

% compute boundary of german map
bound = boundary(data.xCoords, data.yCoords);
data.boundaryX = data.xCoords(bound);
data.boundaryY = data.yCoords(bound);
    
% comput indices of urban and rural area
urbanCmp = strcmp('Urban', data.values(3,:));
data.urbanIndices = find(urbanCmp);
data.ruralIndices = find(~urbanCmp);

% comput region indices
data.easternIndices = find(strcmp('East', data.values(5,:)));
data.nonEsternIndices = find(~strcmp('East', data.values(5,:)));
data.westernIndices = find(strcmp('West', data.values(5,:)));
data.northernIndices = find(strcmp('North', data.values(5,:)));
data.southernIndices = find(strcmp('South', data.values(5,:)));

%% prepare config
% SIP optimziation configs
config.constraintTypeNames = {'GeoAdjacency', 'GeoAdjacency2nd' 'UrbanRural', 'EastAndNonEast', 'Regional'};
%prepare search grids
geoAdjacencyLambdaGrid = containers.Map(data.featureGroupNames, {0.0020422: 1e-07 : 0.0020424, 0.0079294: 1e-07 : 0.0079296, 0.0032196:1e-07:0.0032198});
geoAdjacencyMuGrid = containers.Map(data.featureGroupNames, {0.3695789: 1e-07 : 0.3695791, 0.1561098: 1e-07 : 0.1561100, 0.2332479:1e-07:0.2332481});

geoAdjacency2ndLambdaGrid = containers.Map(data.featureGroupNames, {0.0018784:1e-07:0.0018786,0.0035157:1e-07:0.0035159,0.0023396:1e-07:0.0023398});
geoAdjacency2ndMuGrid = containers.Map(data.featureGroupNames, {0.0446991:1e-07:0.0446993,0.0235759:1e-07:0.0235761,0.0326093:1e-07:0.0326095});
urbanRuralConstLambdasGrid = containers.Map(data.featureGroupNames, {0.0055248:1e-07:0.0055250, 0.0043630: 1e-07 : 0.0043632, 0.0045526: 1e-07 : 0.0045529});
urbanRuralConstMuGrid = containers.Map(data.featureGroupNames, {0.0021439:1e-07:0.0021441, 0.0027202: 1e-07 : 0.0027204, 0.0025833: 1e-07: 0.0025835});

eastNonEastLambdaGrid = containers.Map(data.featureGroupNames, {0.0045430:1e-07:0.0045432,0.0065551:1e-07:0.0065553,0.0044995:1e-07:0.0044997});
eastNonEastMuGrid = containers.Map(data.featureGroupNames, {0.0025837:1e-07:0.0025839, 0.0018967:1e-07:0.0018969,0.0026132:1e-07:0.0026134});

regionalLambdaGrid = containers.Map(data.featureGroupNames, {0.0067041:1e-07:0.0067043,0.0092108:1e-07:0.0092110, 0.0077005:1e-07:0.0077007});
regionalMuGrid = containers.Map(data.featureGroupNames, {0.0087448:1e-07:0.0087450,0.0050544:1e-07:0.0050546, 0.0064990:1e-07:0.0064992});

%non zero scored search grids
% geoAdjacencyLambdaGrid = 2.627e-6:1e-9:2.629e-6 (vote)
% 2.6829e-6:1e-10:2.6831e-6(demographic)
% geoAdjacencyMuGrid = 0.010883162:1e-9:0.010883165 (vote)
% 0.0640735:1e-7:0.0640737 (demographic)
%geoAdjacencyLambdaGrid = containers.Map(data.featureGroupNames, {2.627e-6:1e-9:2.629e-6, 0.0079294: 1e-07 : 0.0079296, 2.6829e-6:1e-10:2.6831e-6});
%geoAdjacencyMuGrid = containers.Map(data.featureGroupNames, {0.010883162:1e-9:0.010883165, 0.1561098: 1e-07 : 0.1561100, 0.0640735:1e-7:0.0640737});

% UrbanRuralLambdaGrid = 0.0000053572:1e-10:0.0000053574 (vote)
% 2.68289e-6:1e-10:2.68291e-6 (demographic)
% UrbanRuralMuGrid  = 0.000053126:1e-9:0.000053128 (vote)
% 0.064072:1e-6:0.064074(demographic)

% EastNonEastLambdaGird = 5.2736e-6:1e-10:5.2738e-6(vote)
%5.3577e-6:1e-10:5.3579e-6 (demographic)
% EastNonEastMuGrid = 7.7598e-5:1e-9:7.7600e-5(vote)
%7.1332e-4:1e-8:7.1334e-4(demographic)
%eastNonEastLambdaGrid = containers.Map(data.featureGroupNames, {5.2736e-6:1e-10:5.2738e-6,0.0065551:1e-07:0.0065553,5.3577e-6:1e-10:5.3579e-6});
%eastNonEastMuGrid = containers.Map(data.featureGroupNames, {7.7598e-5:1e-9:7.7600e-5, 0.0018967:1e-07:0.0018969,7.1332e-4:1e-8:7.1334e-4});

% RegionalLambdaGrid = 1.03826e-5:1e-10:1.03828e-5(vote)
%1.0706e-5:1e-9:1.0708e-5 (demographic)
% RegionalMuGrid = 2.92178e-4:1e-9:2.92180e-4(vote) 
%1.8229e-3:1e-7:1.8231e-3 (demographic)

config.searchGridLambda = containers.Map(config.constraintTypeNames, {geoAdjacencyLambdaGrid, geoAdjacency2ndLambdaGrid, urbanRuralConstLambdasGrid, eastNonEastLambdaGrid, regionalLambdaGrid});
config.searchGridMu = containers.Map(config.constraintTypeNames, {geoAdjacencyMuGrid, geoAdjacency2ndMuGrid, urbanRuralConstMuGrid, eastNonEastMuGrid, regionalMuGrid});
%prepare adjacenc matrix Computers
config.adjancencyMatrixComputerMap = containers.Map(config.constraintTypeNames, {@computeAdjacencyMatrixForGeoAdjacencyGraph, @computeAdjacencyMatrixForGeoAdjacencyGraph2ndOrder, @computeAdjacencyMatrixForUrbanRuralGraph, @computeAdjacencyMatrixForEastAndNonEastGraph, @computeAdjacencyMatrixForRegionalGraph});
config.plotterNameMap = containers.Map(config.constraintTypeNames, {...
    {'3dScatter', '2dScatterAllPos', '2dScatterAllNeg'}, ... 
    {'3dScatter', '2dScatterAllPos', '2dScatterAllNeg'}, ... 
    {'3dScatter','2dScatterUrban', '2dScatterUrbanNeg','2dScatterRural', '2dScatterRuralNeg', '2dScatterAllPos', '2dScatterAllNeg'},...
    {'3dScatter','2dScatterUrban', '2dScatterUrbanNeg','2dScatterRural', '2dScatterRuralNeg', '2dScatterAllPos', '2dScatterAllNeg'},...
    {'3dScatter','2dScatterUrban', '2dScatterUrbanNeg','2dScatterRural', '2dScatterRuralNeg', '2dScatterAllPos', '2dScatterAllNeg'}});

% reduction methods
config.reductionMethodNames = {'pca', 'sip'};

% plot color sheme
config.colorScheme = 'jet';
end