function [w, v, attributeSI] = siDisplacement(inds, target, sallObject)    
    % Because the starting index differ 1 between java and matlab.
    inds = inds + 1;
    % convert java input into arry of objects. 
    target = cell2mat(target');
    sall = constructEquiClasses(sallObject);
    % compute extension of the target data.
    ext = target(inds,:);
    [n, k] = size(ext);      

    % compute background distribution parameters
    mu = zeros(k, 1);
    S = zeros(k,k);
    for s = sall
        int = intersect(s.inds, inds);
        if ~isempty(int)
            mu = mu + length(int)*s.mu;
            S = S + length(int)*s.S;
        end
    end    
    mu = mu/n;
    y = mean(ext,1);
    w = zeros(1, k);
    S = S/n;
%     v = 1./2*log((2*pi)^k*det(S)) - k/2*log(n) + 1./2*(y-mu')/(S./n)*(y'-mu);
    E = eigs(S);    
    v = k/2*log(2*pi) + 1/2*sum(log(E)) - k/2*log(n) + 1./2*(y-mu')/S*n*(y'-mu);
    
    attributeSI = zeros(k ,1);
    for i = 1:k 
        e = zeros(k,1); 
        e(i) = 1;
%         attributeSI(i, 1) = 1./2*log((2*pi)^k*e'*S*e/n/n)+ 1./2*((y-mu')*e)^2/(e'*S*e/n/n);
        attributeSI(i, 1) = 1./2*log((2*pi)^k*e'*S*e/n)+ 1./2*((y-mu')*e)^2/(e'*S*e/n);
    end    
end