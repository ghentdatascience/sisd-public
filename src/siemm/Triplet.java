package siemm;

public class Triplet {
	public double[] w;
	public double[] attrVs;
	public float v;	
	
	public Triplet(double[] w, float v, double[] attrVs) {		
		this.w = w;
		this.v = v;		
		this.attrVs = attrVs;
	}
}
