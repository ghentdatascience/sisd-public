package siemm.waterQuality;

import java.io.*;
import java.util.*;

import nl.liacs.subdisc.*;
import siemm.*;

public class spreadCaseStudy {
	public static void main(String[] args) throws FileNotFoundException {			
		Table table = new FileLoaderARFF(new File("src/siemm/waterQuality/wq.arff")).getTable();
		table.update();
		
		int[] targetIdxs = new int[16];
		for (int i = 0; i< 16; i++) {
			targetIdxs[i] = i; 
		}

		SearchParameters searchParameters = configSearchParameters(table, targetIdxs);		
		Miner miner = new Miner(table, targetIdxs, searchParameters, "src/siemm/waterQuality/spreadPatterns.m");
		miner.mineTwoStepSpreadPatterns(3);	
	}		

	private static SearchParameters configSearchParameters(Table table, int[] targetIdxs) {
		SearchParameters searchParameters = new SearchParameters();
		searchParameters.setTargetConcept(configTargetConcept(table, targetIdxs));			
		
		searchParameters.setSearchDepth(4);
		searchParameters.setMinimumCoverage(10);
		searchParameters.setQualityMeasureMinimum((float) 1.0);
		searchParameters.setMaximumCoverageFraction((float) 0.8);
		searchParameters.setMaximumSubgroups(150);
		searchParameters.setMaximumTime((float) 5);		

		searchParameters.setSearchStrategy("beam");
		searchParameters.setSearchStrategyWidth(40);
		searchParameters.setNominalSets(false);
		searchParameters.setNumericStrategy("bins");
//		searchParameters.setNumericStrategy("vm-consecutive-all");
		searchParameters.setNumericOperators("<html>&#8804;, &#8805;</html>");
		searchParameters.setNrBins(5);
		
		return searchParameters;
	}

	private static TargetConcept configTargetConcept(Table table, int[] targetIdxs) {

		TargetConcept targetConcept = new TargetConcept();
		targetConcept.setTargetType("multi-label");
		List<Column> multiTargets = new ArrayList<>();
		for (int idx : targetIdxs) {
			multiTargets.add(table.getColumn(idx));
		}
		
		targetConcept.setMultiTargets(multiTargets);
		return targetConcept;
	}

}
