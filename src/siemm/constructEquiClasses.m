function sallStruct = constructEquiClasses(sallObject)    
    keys = sallObject{1};
    classes = sallObject{2};
    
    for i = 1:length(classes)
        class = classes{i};
        for j = 1:length(class)
            sallStruct(i).(keys{j}) = class{j};
        end
        d = size(sallStruct(i).mu, 2);
        sallStruct(i).mu = sallStruct(i).mu';
        sallStruct(i).Sinvmu = sallStruct(i).Sinvmu';
        sallStruct(i).S = reshape(sallStruct(i).S, d, d)';
        sallStruct(i).Sminhalfinv = reshape(sallStruct(i).Sminhalfinv, d, d)';
    end
end

