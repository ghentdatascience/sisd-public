function sall = selectSubEquiClasses(inds, sallObject) 
    inds = inds + 1;
    sall = constructEquiClasses(sallObject);
    todelete = false(length(sall),1);
    for i=1:length(sall)
        sinds = sall(i).inds;
        % Store the others in a separate structure:
        int = intersect(sinds,inds);
        if ~isempty(int)
            % since databased is projected onto the subgroup,
            % need to update the indices accordingly.
            sall(i).inds = find(ismember(inds, int));
        else
            todelete(i)=true;
        end
    end
    sall(todelete) = [];
end