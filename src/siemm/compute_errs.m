function errs = compute_errs(sall,inds,w,s2,m)

% First compute new equivalence classes.
% sall will contain the remainders of the previous equivalence classes.
% supdate contains the newly created ones, with the parts of the previous
% equivalence classes that overlapped with inds.

supdate = struct('S',{},'Sminhalfinv',{},'mu',{},'Sinvmu',{},'inds',{});
todelete = false(length(sall),1);
for i=1:length(sall)
    sinds = sall(i).inds;
    % Shrink the set of indices to which sall(i) applies:
    dif = setdiff(sinds,inds);
    sall(i).inds = dif;
    if isempty(dif)
        todelete(i)=true;
    end
    
    % Store the others in a separate structure:
    int = intersect(sinds,inds);
    if ~isempty(int)
        supdate(end+1,1) = sall(i);
        supdate(end).inds = int;
    end
end

% Compute errors.

errs = [0 0];
for i=1:length(supdate)
    ni = length(supdate(i).inds);
    mui = supdate(i).mu;
    Si = supdate(i).S;
    errs(1) = errs(1) + ni*(w'*mui-m);
    errs(2) = errs(2) + ni*(w'*Si*w + (m-mui'*w)^2 - s2);
end
